// StartPage ----------------------------------------------------------------------------------------------------------
$(document).ready( function() {
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    $("#date").val(today);
});

$(".login").click(function() {
    $(".form-login").removeClass("hidden");
});

$(".form-login").click(function(event) {
    if (event.target.id == "form-login-div") {
        $(".form-login").addClass("hidden");
    }
});

$(".authentication-failed-div").click(function(event) {
    if(event.target.id == "failed-div") {
        $(".authentication-failed-div").addClass("hidden");
    }
});

$("#find").click(function(event) {
    var ok = true;
    var regex_country = new RegExp("([A_ZА-Я][a-zA-Zа-яА-Я]{2,18})");

    if (!regex_country.test($("#from").val()) || ($("#to").val()) == " INVALID") {
        $("#from").val(" INVALID");
        ok = false;
    }
    if (!regex_country.test($("#to").val()) || ($("#to").val()) == " INVALID") {
        $("#to").val(" INVALID");
        ok = false;
    }
    if (ok) {
        $("#find-form").submit();
    }
});

$("#luggage-price-button").click(function(event) {

    $("#luggage-price-form").submit();
});

$("#from").click(function(event) {

    if($("#from").val() == " INVALID") {
        $("#from").val("");
    }
});

$("#to").click(function(event) {

    if($("#to").val() == " INVALID") {
        $("#to").val("");
    }
});

$("#login").click(function(event) {

    if(!($("#login-field").val() == "" || $("#password-field").val() == "")) {
        $("#login-form").submit();
    }
});

// Registration -------------------------------------------------------------------------------------------------------

$("#registration").click(function(event) {
    var ok = true;
    var regex_phone = new RegExp("([+]?[0-9]{12})");
    var regex_email = new RegExp("^([a-zA-Z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$");
    var regex_login = new RegExp("^[a-zA-Zа-яА-Я0-9_-]{4,16}$");
    var regex_password = new RegExp("^[a-zA-Zа-яА-Я0-9_-]{8,18}$");
    var regex_passport = new RegExp("([a-z]{2}[0-9]{7})");
    var regex_text_field = new RegExp("([a-zA-Zа-яА-Я]{3,16})");

    if (!regex_phone.test($("div#phone input").val())) {
        $("div#phone img").removeClass("hidden");
        ok = false;
    }
    if(!regex_email.test($("div#email input").val())) {
        $("div#email img").removeClass("hidden");
        ok = false;
    }
    if(!regex_login.test($("div#login input").val())) {
        $("div#login img").removeClass("hidden");
        ok = false;
    }
    if(!regex_password.test($("div#password input").val())) {
        $("div#password img").removeClass("hidden");
        ok = false;
    }
    if(!regex_passport.test($("div#passport input").val())) {
        $("div#passport img").removeClass("hidden");
        ok = false;
    }
    if(!regex_text_field.test($("div#first_name input").val())) {
        $("div#first_name img").removeClass("hidden");
        ok = false;
    }
    if(!regex_text_field.test($("div#last_name input").val())) {
        $("div#last_name img").removeClass("hidden");
        ok = false;
    }
    if(!regex_text_field.test($("div#country input").val())) {
        $("div#country img").removeClass("hidden");
        ok = false;
    }
    if (ok) {
        $("#registration-form").submit();
    }
});

// EditProfile --------------------------------------------------------------------------------------------------------

$("#edit-profile").click(function(event) {
    var ok = true;
    var regex_phone = new RegExp("([+]?[0-9]{12})");
    var regex_password = new RegExp("^[a-zA_Zа-яА-Я0-9_-]{6,18}$");
    var regex_passport = new RegExp("([a-z]{2}[0-9]{7})");
    var regex_text_field = new RegExp("([a-zA-Zа-яА-Я]{3,16})");

    if (!regex_phone.test($("div#phone input").val())) {
        $("div#phone img").removeClass("hidden");
        ok = false;
    }
    if(!regex_password.test($("div#password input").val())) {
        $("div#password img").removeClass("hidden");
        ok = false;
    }
    if(!regex_passport.test($("div#passport input").val())) {
        $("div#passport img").removeClass("hidden");
        ok = false;
    }
    if(!regex_text_field.test($("div#first_name input").val())) {
        $("div#first_name img").removeClass("hidden");
        ok = false;
    }
    if(!regex_text_field.test($("div#last_name input").val())) {
        $("div#last_name img").removeClass("hidden");
        ok = false;
    }
    if(!regex_text_field.test($("div#country input").val())) {
        $("div#country img").removeClass("hidden");
        ok = false;
    }
    if (ok) {
        $("#registration-form").submit();
    }
});

// FlightsPage --------------------------------------------------------------------------------------------------------

$("#order").click(function(event) {
    var flight_number = $("input[name='flightNumber']:checked", "#flights").val();
    if (flight_number !== undefined) {
        $("#flights").submit();
    }
});

$("#prev-flights-page").click(function(event) {
    $("#prev-flights-page").removeClass("red-border");
    $("#next-flights-page").removeClass("red-border");
    $.ajax({
        type : "GET",
        url : "Controller",
        dataType : "html",
        data : {
            command : "print_page",
            page : "flights_page",
            navigation : "prev"
        },
        success : function(responseText) {
            $("#flights-table-body").html(responseText);
        },
        error: function(jqXHR, exception) {
            if (jqXHR.status == 404) {
                $("#prev-flights-page").addClass("red-border");
            } else if (jqXHR.status == 500) {
                var redirectURL = jqXHR.getParameter("redirect-url");
                window.location=redirectURL;
            }
        }
    });
});

$("#next-flights-page").click(function(event) {
    $("#prev-flights-page").removeClass("red-border");
    $("#next-flights-page").removeClass("red-border");
    $.ajax({
        type : "GET",
        url : "Controller",
        dataType : "html",
        data : {
            command : "print_page",
            page : "flights_page",
            navigation : "next"
        },
        success : function(responseText) {
            $("#flights-table-body").html(responseText);
        },
        error: function(jqXHR, exception) {
            if (jqXHR.status == 404) {
                $("#next-flights-page").addClass("red-border");
            } else if (jqXHR.status == 500) {
                var redirectURL = jqXHR.getParameter("redirect-url");
                window.location=redirectURL;
            }
        }
    });
});

// TicketPage ---------------------------------------------------------------------------------------------------------

$(".ticket-button").click(function(event) {
    var ok = true;
    var regex_double = new RegExp("(([0-9]{1,2})(.[1-9]{1,5})?)");

    if($("#luggage1-check").is(":checked")) {
        if (!regex_double.test($("#luggage1-volume").val())) {
            $("#luggage1-volume").val(" - ? -")
            ok = false;
        }
        if(!regex_double.test($("#luggage1-weight").val())) {
            $("#luggage1-weight").val(" - ? -");
            ok = false;
        }
    }

    if($("#luggage2-check").is(":checked")) {
        if (!regex_double.test($("#luggage2-volume").val())) {
            $("#luggage2-volume").val(" - ? -")
            ok = false;
        }
        if(!regex_double.test($("#luggage2-weight").val())) {
            $("#luggage2-weight").val(" - ? -");
            ok = false;
        }
    }

    if($("#luggage3-check").is(":checked")) {
        if (!regex_double.test($("#luggage3-volume").val())) {
            $("#luggage3-volume").val(" - ? -")
            ok = false;
        }
        if(!regex_double.test($("#luggage3-weight").val())) {
            $("#luggage3-weight").val(" - ? -");
            ok = false;
        }
    }

    if (ok) {
        $("#ticket-form").submit();
    }
});

$("#luggage1-weight").click(function(event) {

    if($("#luggage1-weight").val() == " - ? -") {
        $("#luggage1-weight").val("");
    }
});

$("#luggage1-volume").click(function(event) {

    if($("#luggage1-volume").val() == " - ? -") {
        $("#luggage1-volume").val("");
    }
});

$("#luggage2-weight").click(function(event) {

    if($("#luggage2-weight").val() == " - ? -") {
        $("#luggage2-weight").val("");
    }
});

$("#luggage2-volume").click(function(event) {

    if($("#luggage2-volume").val() == " - ? -") {
        $("#luggage2-volume").val("");
    }
});

$("#luggage3-weight").click(function(event) {

    if($("#luggage3-weight").val() == " - ? -") {
        $("#luggage3-weight").val("");
    }
});

$("#luggage3-volume").click(function(event) {

    if($("#luggage3-volume").val() == " - ? -") {
        $("#luggage3-volume").val("");
    }
});

// UserPage -----------------------------------------------------------------------------------------------------------

$("#delete-tickets").click(function(event) {
    var ok = true;
    var checkboxes = document.querySelectorAll('input[class=ticket-checkbox]');
    var empty = [].filter.call( checkboxes, function( el ) {
        return !el.checked
    });

    if (checkboxes.length == empty.length) {
        ok = false;
    }
    if(ok) {
        $("#ticket-delete-form").submit();
    }
});

$("#delete-luggage").click(function(event) {
    var ok = true;
    var checkboxes = document.querySelectorAll('input[class=luggage-checkbox]');
    var empty = [].filter.call( checkboxes, function( el ) {
        return !el.checked
    });

    if (checkboxes.length == empty.length) {
        ok = false;
    }
    if(ok) {
        $("#luggage-delete-form").submit();
    }
});

$("#prev-ticket-page").click(function(event) {
    $("#prev-ticket-page").removeClass("red-border");
    $("#next-ticket-page").removeClass("red-border");
    $.ajax({
        type : "GET",
        url : "Controller",
        dataType : "html",
        data : {
            command : "print_page",
            page : "tickets_page",
            navigation : "prev"
        },
        success : function(responseText) {
            $("#ticket-table-body").html(responseText);
        },
        error: function(jqXHR, exception) {
            if (jqXHR.status == 404) {
                $("#prev-ticket-page").addClass("red-border");
            } else if (jqXHR.status == 500) {
                window.location.replace("Controller?command=forward_to_page&forwardPage=view/jsp/error/ServerError.jsp")
            }
        }
    });
});

$("#next-ticket-page").click(function(event) {
    $("#prev-ticket-page").removeClass("red-border");
    $("#next-ticket-page").removeClass("red-border");
    $.ajax({
        type : "GET",
        url : "Controller",
        dataType : "html",
        data : {
            command : "print_page",
            page : "tickets_page",
            navigation : "next"
        },
        success : function(responseText) {
            $("#ticket-table-body").html(responseText);
        },
        error: function(jqXHR, exception) {
            if (jqXHR.status == 404) {
                $("#next-ticket-page").addClass("red-border");
            } else if (jqXHR.status == 500) {
                window.location.replace("Controller?command=forward_to_page&forwardPage=view/jsp/error/ServerError.jsp")
            }
        }
    });
});

$("#prev-luggage-page").click(function(event) {
    $("#prev-luggage-page").removeClass("red-border");
    $("#next-luggage-page").removeClass("red-border");
    $.ajax({
        type : "GET",
        url : "Controller",
        dataType : "html",
        data : {
            command : "print_page",
            page : "luggage_page",
            navigation : "prev"
        },
        success : function(responseText) {
            $("#luggage-table-body").html(responseText);
        },
        error: function(jqXHR, exception) {
            if (jqXHR.status == 404) {
                $("#prev-luggage-page").addClass("red-border");
            } else if (jqXHR.status == 500) {
                window.location.replace("Controller?command=forward_to_page&forwardPage=view/jsp/error/ServerError.jsp")
            }
        }
    });
});

$("#next-luggage-page").click(function(event) {
    $("#prev-luggage-page").removeClass("red-border");
    $("#next-luggage-page").removeClass("red-border");
    $.ajax({
        type : "GET",
        url : "Controller",
        dataType : "html",
        data : {
            command : "print_page",
            page : "luggage_page",
            navigation : "next"
        },
        success : function(responseText) {
            $("#luggage-table-body").html(responseText);
        },
        error: function(jqXHR, exception) {
            if (jqXHR.status == 404) {
                $("#next-luggage-page").addClass("red-border");
            } else if (jqXHR.status == 500) {
                window.location.replace("Controller?command=forward_to_page&forwardPage=view/jsp/error/ServerError.jsp")
            }
        }
    });
});



