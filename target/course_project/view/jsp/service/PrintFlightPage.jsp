<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:forEach items="${ticketPrices}" var="ticketPrice">
  <tr>
    <td>${ticketPrice.flight.id}</td>
    <td>${ticketPrice.flight.departureTime}</td>
    <td>${ticketPrice.flight.arrivalTime}</td>
    <td>${sessionScope.classType.value()}</td>
    <c:if test="${sessionScope.privilege == true}">
      <td>${ticketPrice.privilegeRBPrice}</td>
    </c:if>
    <td>${ticketPrice.price}</td>
    <td>
      <input type="radio" name="flightNumber" value="${ticketPrice.flight.id}">
    </td>
  </tr>
</c:forEach>