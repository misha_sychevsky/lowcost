<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:forEach items="${tickets}" var="ticket" varStatus="loop">
    <tr>
        <td>${ticket.flight.id}</td>
        <td>${ticket.classType.value()}</td>
        <td>${ticket.seat}</td>
        <td>${ticket.flight.from} - ${ticket.flight.to}</td>
        <td>${ticket.flight.departureTime} - ${ticket.flight.arrivalTime}</td>
        <td>${ticket.flight.date}</td>
        <c:choose>
            <c:when test="${ticket.privilegeRB == true}">
                <td>Yes</td>
            </c:when>
            <c:otherwise>
                <td>No</td>
            </c:otherwise>
        </c:choose>
        <td>${ticket.price} $</td>
        <td>
            <input type="hidden" name="flight-id-${loop.count}" value="${ticket.flight.id}">
            <input class="ticket-checkbox" type="checkbox" name="${loop.count}-ticket-check"><span class="checkbox-text">delete</span>
        </td>
    </tr>
</c:forEach>


