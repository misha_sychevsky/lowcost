<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:forEach items="${luggage}" var="luggageItem" varStatus="loop">
  <tr>
    <td>${luggageItem.id}</td>
    <td>${luggageItem.weight}</td>
    <td>${luggageItem.volume}</td>
    <td>${luggageItem.flight.id}</td>
    <td>${luggageItem.flight.from} - ${luggageItem.flight.to}</td>
    <td>${luggageItem.flight.date}</td>
    <td>${luggageItem.price} $</td>
    <td>
      <input type="hidden" name="luggage-id-${loop.count}" value="${luggageItem.id}">
      <input class="luggage-checkbox" type="checkbox" name="${loop.count}-luggage-check"><span class="checkbox-text">check</span>
    </td>
  </tr>
</c:forEach>
