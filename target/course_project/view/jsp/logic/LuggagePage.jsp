<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib.tld" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <meta charset="utf-8">
    <title><tag:get-property uri="jsp.text.page.title.luggage"/></title>
    <link rel="icon" href="view/pictures/logo.ico">
    <link rel="stylesheet" href="view/css/Main.css">
    <link rel="stylesheet" href="view/css/Table.css">
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
  </head>
  <body>
    <div class="main-container">
      <div class="bar">
        <jsp:include page="../service/LocalizationSelect.jsp"/>
      </div>
      <div id="luggage-logo">
        <img id="luggage-img" src="view/pictures/luggage-d.png">
        <div class="title-container">
          <p id="luggage-title"><tag:get-property uri="jsp.text.logo.title.luggage"/></p>
          <p id="luggage-text"><tag:get-property uri="jsp.text.logo.title.prices"/></p>
        </div>
      </div>
      <div class="luggage-panes-container">
        <div class="table-pane">
          <table class="simple-little-table luggage-table">
            <tr>
              <th><tag:get-property uri="jsp.text.form.volume"/></th>
              <th><tag:get-property uri="jsp.text.form.weight"/></th>
              <th><tag:get-property uri="jsp.text.table.price"/></th>
            </tr>
            <c:forEach items="${priceList}" var="luuggagePrice">
              <tr>
                <td>${luuggagePrice.minVolume} - ${luuggagePrice.maxVolume}</td>
                <td>${luuggagePrice.minWeight} - ${luuggagePrice.maxWeight}</td>
                <td>${luuggagePrice.price}</td>
              </tr>
            </c:forEach>
          </table>
        </div>
        <div class="luggage-info-pane">
          <p class="luggage-info-title">
            <tag:get-property uri="jsp.text.luggage.info.1"/>
          </p>
          <p class="luggage-info-text">
            <tag:get-property uri="jsp.text.luggage.info.2"/>
          </p>
          <p class="luggage-info-text">
            <tag:get-property uri="jsp.text.luggage.info.3"/>
          </p>
          <p class="luggage-info-text">
            <tag:get-property uri="jsp.text.luggage.info.4"/>
          </p>
          <p class="luggage-info-text">
            <tag:get-property uri="jsp.text.luggage.info.5"/>
          </p>
          <p class="luggage-info-text">
            <tag:get-property uri="jsp.text.luggage.info.6"/>
          </p>
          <p class="luggage-info-text">
            <tag:get-property uri="jsp.text.luggage.info.7"/>
          </p>
        </div>
      </div>
      <a href="../../../StartPage.jsp" id="main-ref">LowCosteR</a>
    </div>
    <script src="view/js/scripts.js"></script>
  </body>
</html>
