<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib.tld" %>
<html>
  <head>
    <title><tag:get-property uri="jsp.text.page.title.tickets"/></title>
    <link rel="icon" href="view/pictures/logo.ico">
    <link rel="stylesheet" href="view/css/Main.css">
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
  </head>
  <body>
    <c:if test="${flag != null && flag == 'ticket_order_failed'}">
      <div class="authentication-failed-div" id="failed-div">
        <div class="authentication-failed">
          <img class="failed-img" src="view/pictures/warning38.png">
          <p class="failed-text ">
            <tag:get-property uri="jsp.text.luggage-register.error.1"/>
          </p>
          <p class="failed-text">
            <tag:get-property uri="jsp.text.luggage-register.error.2"/>
          </p>
          <p class="failed-text">
            <tag:get-property uri="jsp.text.luggage-register.error.3"/>
          </p>
        </div>
      </div>
    </c:if>
    <c:if test="${flag != null && flag == 'ticket_already_exists'}">
      <div class="authentication-failed-div" id="failed-div">
        <div class="authentication-failed">
          <img class="failed-img" src="view/pictures/warning38.png">
          <p class="failed-text ">
            <tag:get-property uri="jsp.text.ticket-order.error.1"/>
          </p>
          <p class="failed-text">
            <tag:get-property uri="jsp.text.ticket-order.error.2"/>
          </p>
        </div>
      </div>
    </c:if>
    <div class="main-container">
      <div class="bar">
        <jsp:include page="../service/LocalizationSelect.jsp"/>
        <jsp:include page="../service/AccountNavigate.jsp"/>
      </div>
      <div class="logo-flight">
        <img class="logo-img flight-img" src="view/pictures/flight.png">
        <div class="flight-title-container">
          <p class="logo-text-title"><tag:get-property uri="jsp.text.logo.title.flights"/></p>
          <p class="logo-text"><tag:get-property uri="jsp.text.logo.title.tickets"/></p>
        </div>
      </div>
      <div class="ticket-container">
        <div class="ticket-info">
          <div>
            <p class="ticket-field-name"><tag:get-property uri="jsp.text.header.flight"/></p>
            <p class="ticket-field-name ticket-field-text">${sessionScope.ticketPrice.flight.id}</p>
          </div>
          <div>
            <p class="ticket-field-name"><tag:get-property uri="jsp.text.header.from"/></p>
            <p class="ticket-field-name ticket-field-text">${sessionScope.ticketPrice.flight.from}</p>
          </div>
          <div>
            <p class="ticket-field-name"><tag:get-property uri="jsp.text.header.to"/></p>
            <p class="ticket-field-name ticket-field-text">${sessionScope.ticketPrice.flight.to}</p>
          </div>
          <div>
            <p class="ticket-field-name"><tag:get-property uri="jsp.text.header.date"/></p>
            <p class="ticket-field-name ticket-field-text">${sessionScope.ticketPrice.flight.date}</p>
          </div>
          <div>
            <p class="ticket-field-name"><tag:get-property uri="jsp.text.header.departure"/></p>
            <p class="ticket-field-name ticket-field-text">${sessionScope.ticketPrice.flight.departureTime}</p>
          </div>
          <div>
            <p class="ticket-field-name"><tag:get-property uri="jsp.text.header.arrival"/></p>
            <p class="ticket-field-name ticket-field-text">${sessionScope.ticketPrice.flight.arrivalTime}</p>
          </div>
          <div>
            <p class="ticket-field-name"><tag:get-property uri="jsp.text.header.class"/></p>
            <p class="ticket-field-name ticket-field-text">${sessionScope.classType.value()}</p>
          </div>
          <div>
            <p class="ticket-field-name"><tag:get-property uri="jsp.text.header.seat"/></p>
            <p class="ticket-field-name ticket-field-text">${sessionScope.ticketPrice.freeSeatsCount}</p>
          </div>
          <div>
            <p class="ticket-field-name"><tag:get-property uri="jsp.text.header.price"/></p>
            <p class="ticket-field-name ticket-field-text">${sessionScope.ticketPrice.price}$</p>
          </div>
          <div>
            <p class="ticket-field-name"><tag:get-property uri="jsp.text.header.privilege"/></p>
            <p class="ticket-field-name ticket-field-text">${sessionScope.ticketPrice.privilegeRBPrice}$</p>
          </div>
        </div>
        <img class="ticket-img" src="view/pictures/ticket-img.png">
      </div>
      <div class="luggage-form-container">
        <form action="Controller" method="post" id="ticket-form">
          <div>
            <div class="luggage-form">
              <div>
                <p class="luggage-form-title"><tag:get-property uri="jsp.text.form.luggage"/> №1</p>
                <span class="luggage-check-text"><input id="luggage1-check" class="luggage-check" type="checkbox" name="1-luggage-check">+</span>
              </div>
              <hr>
              <div>
                <p class="luggage-form-name"><tag:get-property uri="jsp.text.form.volume"/></p>
                <input id="luggage1-volume" type="text" class="luggage-form-text" name="1-luggage-volume">
              </div>
              <div>
                <p class="luggage-form-name"><tag:get-property uri="jsp.text.form.weight"/></p>
                <input id="luggage1-weight" type="text" class="luggage-form-text" name="1-luggage-weight">
              </div>
            </div>
            <div class="luggage-form">
              <div>
                <p class="luggage-form-title"><tag:get-property uri="jsp.text.form.luggage"/> №2</p>
                <span class="luggage-check-text"><input id="luggage2-check" class="luggage-check" type="checkbox" name="2-luggage-check">+</span>
              </div>
              <hr>
              <div>
                <p class="luggage-form-name"><tag:get-property uri="jsp.text.form.volume"/></p>
                <input id="luggage2-volume" type="text" class="luggage-form-text" name="2-luggage-volume">
              </div>
              <div>
                <p class="luggage-form-name"><tag:get-property uri="jsp.text.form.weight"/></p>
                <input id="luggage2-weight" type="text" class="luggage-form-text" name="2-luggage-weight">
              </div>
            </div>
            <div class="luggage-form">
              <div>
                <p class="luggage-form-title"><tag:get-property uri="jsp.text.form.luggage"/> №3</p>
                <span class="luggage-check-text"><input id="luggage3-check" class="luggage-check" type="checkbox" name="3-luggage-check">+</span>
              </div>
              <hr>
              <div>
                <p class="luggage-form-name"><tag:get-property uri="jsp.text.form.volume"/></p>
                <input id="luggage3-volume" type="text" class="luggage-form-text" name="3-luggage-volume">
              </div>
              <div>
                <p class="luggage-form-name"><tag:get-property uri="jsp.text.form.weight"/></p>
                <input id="luggage3-weight" type="text" class="luggage-form-text" name="3-luggage-weight">
              </div>
            </div>
          </div>
          <input type="hidden" name="command" value="ticket_order"/>
          <div class="login-button ticket-button">
            <tag:get-property uri="jsp.text.form.order.button"/>
          </div>
        </form>
      </div>
      <a href="../../../StartPage.jsp" id="main-ref">LowCosteR</a>
    </div>
    <script src="view/js/scripts.js"></script>
  </body>
</html>