package by.bsuir.util.tag;

import by.bsuir.business.service.CommandConstants;
import by.bsuir.util.global.BundleControl;
import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Tag gets property from user taglib.
 * This class is necessary to get the bundle property for jsp page
 * @author Michail Sychevsky
 */
public class GetPropertyTag extends TagSupport {

    private static final String RESOURCE_PATH = "localization";
    private static final String DEFAULT_LOCALE = "EN";
    private static final Logger LOGGER = Logger.getLogger(GetPropertyTag.class);

    private String uri;

    public void setUri(String uri)
    {
        this.uri = uri;
    }

    @Override
    public int doStartTag() throws JspException
    {
        String language = (String) pageContext.getSession().getAttribute(CommandConstants.PARAMETER_LANGUAGE);
        Locale locale;
        if(language == null)
        {
            locale = new Locale(DEFAULT_LOCALE);
        }
        else
        {
            locale = new Locale(language);
        }

        ResourceBundle resource = ResourceBundle.getBundle(RESOURCE_PATH, locale, new BundleControl());
        JspWriter writer = pageContext.getOut();
        try
        {
            try
            {
                writer.print(resource.getString(uri));
            }
            catch (MissingResourceException ex)
            {
                LOGGER.error(ex);
            }
        }
        catch (IOException ex)
        {
            LOGGER.error(ex);
        }

        return SKIP_BODY;
    }
}
