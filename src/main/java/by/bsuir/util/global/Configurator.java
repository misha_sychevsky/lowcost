package by.bsuir.util.global;

import java.util.ResourceBundle;

/**
 * This class get the global application property from properties
 * @author Michail Sychevsky
 */
public class Configurator {

    private static final String GLOBAL_RESOURCE_PATH = "global";

    private static ResourceBundle globalProperties = ResourceBundle.getBundle(GLOBAL_RESOURCE_PATH, new BundleControl());

    public static String getGlobalProperty(String name)
    {
        return globalProperties.getString(name);
    }
}
