package by.bsuir.business.service;

import by.bsuir.business.command.*;
import by.bsuir.business.command.account.*;
import by.bsuir.business.command.logic.*;
import by.bsuir.business.command.navigation.global.ForwardToPage;
import by.bsuir.business.command.navigation.table.PrintPage;
import by.bsuir.business.command.settings.ChangeLanguage;
import by.bsuir.business.exception.NoSuchCommandException;

/**
 * This is class which implements a pattern factory
 * This class get a commandName from request and return a instance of class <code>Command</code>
 * @author Michail Sychevsky
 */
public class CommandFactory {

    /**
     * This method get a commandName from request and return a instance of class <code>Command</code>
     * @param commandName - name of the command.
     * @return a new <code>Command</code>, this a instance of class which implements
     * a pattern command
     */
    public static Command getCommand(String commandName) throws NoSuchCommandException
    {
        switch (identifyCommand(commandName))
        {
            case SHOW_LUGGAGE_INFORMATION:
                return new ShowLuggageInformation();

            case FLIGHTS_SEARCH:
                return new FlightsSearch();

            case SHOW_SELECTED_TICKET:
                return new ShowSelectedTicket();

            case REGISTRATION:
                return new Registration();

            case FORWARD_TO_PAGE:
                return new ForwardToPage();

            case AUTHORIZATION:
                return new Authorization();

            case HOME:
                return new Home();

            case EDIT_PROFILE:
                return new EditProfile();

            case EXIT:
                return new Exit();

            case TICKET_ORDER:
                return new TicketOrder();

            case DELETE_TICKETS:
                return new DeleteTickets();

            case DELETE_LUGGAGE:
                return new DeleteLuggage();

            case PRINT_PAGE:
                return new PrintPage();

            case CHANGE_LANGUAGE:
                return new ChangeLanguage();

            case UNKNOWN_COMMAND:
                throw new NoSuchCommandException();

            default:
                throw new NoSuchCommandException();
        }
    }

    public static Commands identifyCommand(String commandName)
    {
        Commands command;
        try
        {
            command = Commands.valueOf(commandName.toUpperCase());
        }
        catch (IllegalArgumentException e)
        {
            command = Commands.UNKNOWN_COMMAND;
        }
        return command;
    }
}
