package by.bsuir.business.service;

/**
 * This enum contains enumeration of all commands
 * @author Michail Sychevsky
 */
public enum Commands {

    UNKNOWN_COMMAND, SHOW_LUGGAGE_INFORMATION, FLIGHTS_SEARCH,
    SHOW_SELECTED_TICKET, REGISTRATION, AUTHORIZATION, HOME,
    EDIT_PROFILE ,EXIT, FORWARD_TO_PAGE, TICKET_ORDER,
    DELETE_TICKETS, DELETE_LUGGAGE, PRINT_PAGE, CHANGE_LANGUAGE
}
