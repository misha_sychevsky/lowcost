package by.bsuir.business.service;

/**
 * This enum contains all the constants used by the commands.
 * @author Michail Sychevsky
 */
public class CommandConstants {

    public static final String PARAMETER_FROM_CITY = "from";
    public static final String PARAMETER_TO_CITY = "to";
    public static final String PARAMETER_DATE = "date";
    public static final String PARAMETER_CLASS = "classType";
    public static final String PARAMETER_PRIVILEGE = "privilege";
    public static final String PARAMETER_FLIGHT_NUMBER = "flightNumber";
    public static final String PARAMETER_FIRST_NAME = "firstName";
    public static final String PARAMETER_LAST_NAME = "lastName";
    public static final String PARAMETER_BIRTH_DATE = "birthDate";
    public static final String PARAMETER_PHONE = "phone";
    public static final String PARAMETER_E_MAIL = "email";
    public static final String PARAMETER_LOGIN = "login";
    public static final String PARAMETER_PASSWORD = "password";
    public static final String PARAMETER_PASSPORT = "passport";
    public static final String PARAMETER_COUNTRY = "country";
    public static final String PARAMETER_FORWARD_PAGE = "forwardPage";
    public static final String PARAMETER_COMMAND = "command";
    public static final String PARAMETER_PAYED = "payed";
    public static final String PARAMETER_LUGGAGE_CHECK = "luggage-check";
    public static final String PARAMETER_LUGGAGE_WEIGHT = "luggage-weight";
    public static final String PARAMETER_LUGGAGE_VOLUME = "luggage-volume";
    public static final String PARAMETER_FLIGHT_ID = "flight-id";
    public static final String PARAMETER_LUGGAGE_ID = "luggage-id";
    public static final String PARAMETER_TICKET_CHECK = "ticket-check";
    public static final String PARAMETER_PAGE = "page";
    public static final String PARAMETER_NAVIGATION = "navigation";
    public static final String PARAMETER_LANGUAGE = "language";

    public static final int LUGGAGE_REGISTERING_COUNT = 3;
    public static final int TABLE_TICKETS_ITEMS_COUNT_ON_PAGE = 8;
    public static final int TABLE_LUGGAGE_ITEMS_COUNT_ON_PAGE = 8;
    public static final int TABLE_FLIGHTS_ITEMS_COUNT_ON_PAGE = 8;
    public static final int FIRST_PAGE = 1;
    public static final int NO_ITEMS = 0;

    public static final String ATTRIBUTE_TICKET_PRICES = "ticketPrices";
    public static final String ATTRIBUTE_TICKET_PRICE = "ticketPrice";
    public static final String ATTRIBUTE_LUGGAGE_PRICES = "priceList";
    public static final String ATTRIBUTE_CLIENT = "client";
    public static final String ATTRIBUTE_TICKETS = "tickets";
    public static final String ATTRIBUTE_LUGGAGE = "luggage";
    public static final String ATTRIBUTE_FLAG = "flag";
    public static final String ATTRIBUTE_TICKETS_FLAG = "tickets_flag";
    public static final String ATTRIBUTE_LUGGAGE_FLAG = "luggage_flag";
    public static final String ATTRIBUTE_TICKETS_PAGE = "tickets_page";
    public static final String ATTRIBUTE_LUGGAGE_PAGE = "luggage_page";
    public static final String ATTRIBUTE_FLIGHTS_PAGE = "flights_page";
    public static final String ATTRIBUTE_LAST_TICKETS_PAGE = "last_ticket_page";
    public static final String ATTRIBUTE_LAST_LUGGAGE_PAGE = "last_luggage_page";
    public static final String ATTRIBUTE_LAST_FLIGHTS_PAGE = "last_flights_page";
    public static final String ATTRIBUTE_EXCEPTION = "exception";

    public static final String FLAG_REGISTRATION_SUCCESSFUL = "registration_successful";
    public static final String FLAG_REGISTRATION_FAILED = "registration_failed";
    public static final String FLAG_EDIT_SUCCESSFUL = "editing_successful";
    public static final String FLAG_EDIT_FAILED = "editing_failed";
    public static final String FLAG_AUTHORIZATION_FAILED = "authorization_failed";
    public static final String FLAG_SUCH_LOGIN_EXISTS = "login_exists";
    public static final String FLAG_TICKET_ORDER_FAILED = "ticket_order_failed";
    public static final String FLAG_TICKET_ORDER_SUCCESSFUL = "ticket_order_successful";
    public static final String FLAG_TICKET_ALREADY_EXISTS = "ticket_already_exists";
    public static final String FLAG_NO_TICKETS = "no_tickets";
    public static final String FLAG_NO_LUGGAGE = "no_luggage";
    public static final String FLAG_NO_FLIGHTS = "no_flights";
    public static final String FLAG_LUGGAGE_DELETE_ERROR = "luggage_delete_error";
    public static final String FLAG_TICKET_DELETE_ERROR = "ticket_delete_error";

    public static final String FORWARD_FLIGHTS_PAGE = "forward_flights_page";
    public static final String FORWARD_TICKET_PAGE = "forward_ticket_page";
    public static final String FORWARD_START_PAGE = "forward_start_page";
    public static final String FORWARD_LUGGAGE_PAGE = "forward_luggage_page";
    public static final String FORWARD_REGISTRATION_PAGE = "forward_registration_page";
    public static final String FORWARD_SERVER_ERROR = "forward_server_error";
    public static final String FORWARD_CLIENT_PAGE = "forward_client_page";
    public static final String FORWARD_EDIT_PAGE = "forward_edit_page";
    public static final String FORWARD_PRINT_TICKET_PAGE = "forward_print_ticket_page";
    public static final String FORWARD_PRINT_LUGGAGE_PAGE = "forward_print_luggage_page";
    public static final String FORWARD_PRINT_FLIGHT_PAGE = "forward_print_flight_page";
    public static final String FORWARD_NOT_FOUND_PAGE = "forward_not_found_page";
    public static final String FORWARD_CONTROLLER = "forward_controller";

    public static final String USE_DATABASE = "use_database";

    public static final String UPDATER_PERIOD = "price.updater.period";
    public static final String UPDATER_DAY_DEADLINE = "price.updater.day-deadline";
    public static final String UPDATER_SEATS_BARRIER_COUNT = "price.updater.seats-barrier-count";
    public static final String UPDATER_MARGIN = "price.updater.margin";
}
