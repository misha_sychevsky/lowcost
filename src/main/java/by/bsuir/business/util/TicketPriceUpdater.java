package by.bsuir.business.util;

import by.bsuir.business.service.CommandConstants;
import by.bsuir.data.database.dao.AbstractDAOFactory;
import by.bsuir.data.database.dao.framework.TicketPriceDAO;
import by.bsuir.data.database.exception.DAOException;
import by.bsuir.data.database.exception.UnsupportedDatabaseException;
import by.bsuir.data.model.entity.TicketPrice;
import by.bsuir.util.global.Configurator;
import org.apache.log4j.Logger;


import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * This class is a daemon which update ticket prices by date and free seats count.
 * @author Michail Sychevsky
 */
public class TicketPriceUpdater extends TimerTask {

    private static final Logger LOGGER = Logger.getLogger(TicketPriceUpdater.class);

    private Timer timer;
    private long period;
    private int dateDeadline;
    private int seatsBarrierCount;
    private double margin;

    public TicketPriceUpdater()
    {
        period = Long.parseLong(Configurator.getGlobalProperty(CommandConstants.UPDATER_PERIOD));
        dateDeadline = Integer.parseInt(Configurator.getGlobalProperty(CommandConstants.UPDATER_DAY_DEADLINE));
        seatsBarrierCount = Integer.parseInt(Configurator.getGlobalProperty(CommandConstants.UPDATER_SEATS_BARRIER_COUNT));
        margin = Integer.parseInt(Configurator.getGlobalProperty(CommandConstants.UPDATER_MARGIN));
    }

    /**
     * This method check the dates of all flights and the number of free places and if necessary price increases.
     */
    @Override
    public void run()
    {
        Date currentDate = new Date(System.currentTimeMillis());

        try
        {
            TicketPriceDAO dao = AbstractDAOFactory.getDAOFactory(Configurator.getGlobalProperty(CommandConstants.USE_DATABASE)).getTicketPriceDAO();
            List<TicketPrice> ticketPrices = dao.getAllTicketPrices();

            for(TicketPrice price : ticketPrices)
            {
                long difference = price.getFlight().getDate().getTime() - currentDate.getTime();
                double doubleDays = (double) difference/(24 * 60 * 60 * 1000);
                if((double) difference % (24 * 60 * 60 * 1000) != 0)
                {
                    doubleDays++;
                }

                int days = (int) doubleDays;
                double upPrice = price.getPrice();
                boolean update = false;
                if(days == dateDeadline)
                {
                    upPrice += margin;
                    update = true;
                }
                if(price.getFreeSeatsCount() == seatsBarrierCount)
                {
                    upPrice += margin;
                    update = true;
                }

                if(update)
                {
                    dao.updateTicketPrice(price.getFlight().getId(), price.getClassType(), upPrice);
                }
            }

        }
        catch (UnsupportedDatabaseException | DAOException e)
        {
            LOGGER.error(e);
        }
    }

    /**
     * Starts the task of daemon
     */
    public void start()
    {
        if (null == timer )
        {
            timer = new Timer(true);
            timer.schedule(this, 0, period);
        }
    }

    /**
     * Stop the task of daemon
     */
    public void stop()
    {
        timer.cancel();
    }
}
