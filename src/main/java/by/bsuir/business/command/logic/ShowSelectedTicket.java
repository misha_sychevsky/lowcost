package by.bsuir.business.command.logic;

import by.bsuir.business.service.CommandConstants;
import by.bsuir.business.command.Command;
import by.bsuir.data.database.dao.AbstractDAOFactory;
import by.bsuir.data.database.dao.framework.TicketPriceDAO;
import by.bsuir.data.database.exception.DAOException;
import by.bsuir.data.database.exception.UnsupportedDatabaseException;
import by.bsuir.data.model.entity.TicketPrice;
import by.bsuir.data.model.type.ClassType;
import by.bsuir.util.global.Configurator;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * This class implements a pattern command
 * This class show the information about the ticket of checked flight
 * @author Michail Sychevsky
 */
public class ShowSelectedTicket implements Command {

    public static final Logger LOGGER = Logger.getLogger(ShowSelectedTicket.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            HttpSession session = request.getSession();
            ClassType classType = (ClassType) session.getAttribute(CommandConstants.PARAMETER_CLASS);
            int flightNumber = Integer.parseInt(request.getParameter(CommandConstants.PARAMETER_FLIGHT_NUMBER));

            TicketPriceDAO dao = AbstractDAOFactory.getDAOFactory(Configurator.getGlobalProperty(CommandConstants.USE_DATABASE)).getTicketPriceDAO();
            TicketPrice ticketPrice = dao.getTicketPriceBySelected(flightNumber, classType);

            session.setAttribute(CommandConstants.ATTRIBUTE_TICKET_PRICE, ticketPrice);
            request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_TICKET_PAGE)).forward(request, response);
        }
        catch (UnsupportedDatabaseException | DAOException e)
        {
            LOGGER.error(e);
            request.setAttribute(CommandConstants.ATTRIBUTE_EXCEPTION, DAOException.ERROR_ID);
            request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_SERVER_ERROR)).forward(request, response);
        }
    }
}
