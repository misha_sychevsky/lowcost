package by.bsuir.business.command.logic;

import by.bsuir.business.service.CommandConstants;
import by.bsuir.business.command.Command;
import by.bsuir.data.database.dao.AbstractDAOFactory;
import by.bsuir.data.database.dao.framework.TicketPriceDAO;
import by.bsuir.data.database.exception.DAOException;
import by.bsuir.data.database.exception.UnsupportedDatabaseException;
import by.bsuir.data.model.entity.TicketPrice;
import by.bsuir.data.model.type.ClassType;
import by.bsuir.util.global.Configurator;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.util.List;

/**
 * This class implements a pattern command
 * This class making the search of flights by user parameters
 * @author Michail Sychevsky
 */
public class FlightsSearch implements Command {

    public static final Logger LOGGER = Logger.getLogger(FlightsSearch.class);
    private String forward = Configurator.getGlobalProperty(CommandConstants.FORWARD_FLIGHTS_PAGE);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            String from = request.getParameter(CommandConstants.PARAMETER_FROM_CITY);
            String to = request.getParameter(CommandConstants.PARAMETER_TO_CITY);
            String privilegeValue = request.getParameter(CommandConstants.PARAMETER_PRIVILEGE);
            String classValue = request.getParameter(CommandConstants.PARAMETER_CLASS).toUpperCase();
            String dateValue = request.getParameter(CommandConstants.PARAMETER_DATE);

            boolean privilege = privilegeValue != null;
            Date date = Date.valueOf(dateValue);
            ClassType classType = ClassType.valueOf(classValue);

            HttpSession session = request.getSession();
            session.setAttribute(CommandConstants.PARAMETER_FROM_CITY, from);
            session.setAttribute(CommandConstants.PARAMETER_TO_CITY, to);
            session.setAttribute(CommandConstants.PARAMETER_PRIVILEGE, privilege);
            session.setAttribute(CommandConstants.PARAMETER_CLASS, classType);
            session.setAttribute(CommandConstants.PARAMETER_DATE, date);
            session.setAttribute(CommandConstants.ATTRIBUTE_FLIGHTS_PAGE, CommandConstants.FIRST_PAGE);

            TicketPriceDAO dao = AbstractDAOFactory.getDAOFactory(Configurator.getGlobalProperty(CommandConstants.USE_DATABASE)).getTicketPriceDAO();
            List<TicketPrice> ticketPrices = dao.getTicketPricesPageBySearchParameters(from, to, date, classType, CommandConstants.FIRST_PAGE - 1, CommandConstants.TABLE_FLIGHTS_ITEMS_COUNT_ON_PAGE);

            checkFlightsCount(ticketPrices, request);

            request.getRequestDispatcher(forward).forward(request, response);
        }
        catch (UnsupportedDatabaseException  | DAOException e)
        {
            LOGGER.error(e);
            request.setAttribute(CommandConstants.ATTRIBUTE_EXCEPTION, DAOException.ERROR_ID);
            request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_SERVER_ERROR)).forward(request, response);
        }
    }

    /**
     * This method checks flights count.
     *
     * Check the last flights page or not or page is empty
     * @param ticketPrices
     * @param request
     */
    private void checkFlightsCount(List<TicketPrice> ticketPrices, HttpServletRequest request) throws ServletException, IOException
    {
        if(ticketPrices.size() != CommandConstants.TABLE_FLIGHTS_ITEMS_COUNT_ON_PAGE)
        {
            request.getSession().setAttribute(CommandConstants.ATTRIBUTE_LAST_FLIGHTS_PAGE, true);
        }
        else
        {
            request.getSession().setAttribute(CommandConstants.ATTRIBUTE_LAST_FLIGHTS_PAGE, false);
        }
        if(ticketPrices.size() != CommandConstants.NO_ITEMS)
        {
            request.setAttribute(CommandConstants.ATTRIBUTE_TICKET_PRICES, ticketPrices);
        }
        else
        {
            request.setAttribute(CommandConstants.ATTRIBUTE_FLAG, CommandConstants.FLAG_NO_FLIGHTS);
            forward = Configurator.getGlobalProperty(CommandConstants.FORWARD_START_PAGE);
        }
    }
}
