package by.bsuir.business.command.logic;

import by.bsuir.business.command.Command;
import by.bsuir.business.service.CommandConstants;
import by.bsuir.data.database.dao.AbstractDAOFactory;
import by.bsuir.data.database.dao.framework.LuggageDAO;
import by.bsuir.data.database.dao.framework.TicketDAO;
import by.bsuir.data.database.dao.framework.TicketPriceDAO;
import by.bsuir.data.database.exception.DAOException;
import by.bsuir.data.database.exception.UnsupportedDatabaseException;
import by.bsuir.data.model.builder.LuggageBuilder;
import by.bsuir.data.model.entity.*;
import by.bsuir.data.xml.FileProcessor;
import by.bsuir.data.xml.exception.XMLException;
import by.bsuir.util.global.Configurator;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class implements a pattern command
 * This class is necessary for ticket order.
 * @author Michail Sychevsky
 */
public class TicketOrder implements Command {

    public static final Logger LOGGER = Logger.getLogger(TicketOrder.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            HttpSession session = request.getSession();
            TicketPrice ticketPrice = (TicketPrice) session.getAttribute(CommandConstants.ATTRIBUTE_TICKET_PRICE);
            Client client = (Client) session.getAttribute(CommandConstants.ATTRIBUTE_CLIENT);
            boolean privilege = (Boolean) session.getAttribute(CommandConstants.PARAMETER_PRIVILEGE);

            String forward = Configurator.getGlobalProperty(CommandConstants.FORWARD_START_PAGE);
            boolean success = true;

            List<Luggage> luggageList = new ArrayList<>();
            for(int luggageCount=1; luggageCount <= CommandConstants.LUGGAGE_REGISTERING_COUNT; ++luggageCount)
            {
                String luggageCheck = request.getParameter(luggageCount + "-" + CommandConstants.PARAMETER_LUGGAGE_CHECK);
                if(luggageCheck != null)
                {
                    String weight = request.getParameter(luggageCount + "-" + CommandConstants.PARAMETER_LUGGAGE_WEIGHT);
                    String volume = request.getParameter(luggageCount + "-" + CommandConstants.PARAMETER_LUGGAGE_VOLUME);

                    LuggageBuilder builder = new LuggageBuilder();
                    if(builder.buildLuggage(weight, volume, client, ticketPrice.getFlight()))
                    {
                        Luggage luggage = builder.getLuggage();
                        considerLuggagePrice(luggage);
                        luggageList.add(luggage);
                    }
                    else
                    {
                        forward = Configurator.getGlobalProperty(CommandConstants.FORWARD_TICKET_PAGE);
                        request.setAttribute(CommandConstants.ATTRIBUTE_FLAG, CommandConstants.FLAG_TICKET_ORDER_FAILED);
                        success = false;
                    }
                }
            }

            if(success)
            {
                Ticket ticket = makeTicket(ticketPrice, privilege, client);
                TicketDAO ticketDAO = AbstractDAOFactory.getDAOFactory(Configurator.getGlobalProperty(CommandConstants.USE_DATABASE)).getTicketDAO();
                TicketPriceDAO ticketPriceDAO = AbstractDAOFactory.getDAOFactory(Configurator.getGlobalProperty(CommandConstants.USE_DATABASE)).getTicketPriceDAO();

                if(!ticketDAO.isClientHasTicketOnTheFlight(client, ticketPrice.getFlight()))
                {
                    ticketPriceDAO.decrementFreeSeatsCount(ticketPrice.getFlight().getId(), ticketPrice.getClassType());
                    ticketDAO.insertTicket(ticket);
                    processLuggageRegistration(luggageList);

                    request.setAttribute(CommandConstants.ATTRIBUTE_FLAG, CommandConstants.FLAG_TICKET_ORDER_SUCCESSFUL);
                }
                else
                {
                    forward = Configurator.getGlobalProperty(CommandConstants.FORWARD_TICKET_PAGE);
                    request.setAttribute(CommandConstants.ATTRIBUTE_FLAG, CommandConstants.FLAG_TICKET_ALREADY_EXISTS);
                }
            }
            request.getRequestDispatcher(forward).forward(request, response);
        }
        catch (UnsupportedDatabaseException | XMLException | DAOException e)
        {
            LOGGER.error(e);
            request.setAttribute(CommandConstants.ATTRIBUTE_EXCEPTION, DAOException.ERROR_ID);
            request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_SERVER_ERROR)).forward(request, response);
        }
    }

    /**
     * This method makes ticket to order
     *
     * @param ticketPrice
     * @param privilege
     * @param client
     * @return
     * @throws DAOException
     * @throws UnsupportedDatabaseException
     */
    private Ticket makeTicket(TicketPrice ticketPrice, boolean privilege, Client client) throws DAOException, UnsupportedDatabaseException
    {
        Ticket ticket = new Ticket();
        ticket.setClassType(ticketPrice.getClassType());
        ticket.setFlight(ticketPrice.getFlight());
        ticket.setClient(client);
        ticket.setPrivilegeRB(privilege);
        if(privilege)
        {
            ticket.setPrice(ticketPrice.getPrice() + ticketPrice.getPrivilegeRBPrice());
        }
        else
        {
            ticket.setPrice(ticketPrice.getPrice());
        }
        ticket.setSeat(String.valueOf(ticketPrice.getFreeSeatsCount()));
        return ticket;
    }

    /**
     * This method registers the luggage.
     *
     * @param luggageList
     * @throws UnsupportedDatabaseException
     * @throws DAOException
     */
    private void processLuggageRegistration(List<Luggage> luggageList) throws UnsupportedDatabaseException, DAOException
    {
        LuggageDAO dao = AbstractDAOFactory.getDAOFactory(Configurator.getGlobalProperty(CommandConstants.USE_DATABASE)).getLuggageDAO();
        for(Luggage luggage : luggageList)
        {
            dao.insertLuggage(luggage);
        }
    }

    /**
     * This method consider the price of luggage.
     *
     * @param luggage
     * @throws XMLException
     */
    private void considerLuggagePrice(Luggage luggage) throws XMLException
    {
        List<LuggagePrice> luggagePrices = FileProcessor.getInstance().getPriceList().getLuggagePrices();
        for(LuggagePrice luggagePrice : luggagePrices)
        {
            if(isInRange(luggage, luggagePrice))
            {
                luggage.setPrice(luggagePrice.getPrice());
                break;
            }
        }
    }

    /**
     * This method check the weight and volume range of luggage to consider price.
     *
     * @param luggage
     * @param luggagePrice
     * @return
     */
    private boolean isInRange(Luggage luggage, LuggagePrice luggagePrice)
    {
        boolean result = true;
        result &= luggage.getWeight() >= luggagePrice.getMinWeight();
        result &= luggage.getWeight() <= luggagePrice.getMaxWeight();
        result &= luggage.getVolume() >= luggagePrice.getMinVolume();
        result &= luggage.getVolume() <= luggagePrice.getMaxVolume();
        return result;
    }
}
