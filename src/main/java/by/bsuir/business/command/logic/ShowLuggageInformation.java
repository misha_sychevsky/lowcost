package by.bsuir.business.command.logic;

import by.bsuir.business.service.CommandConstants;
import by.bsuir.business.command.Command;
import by.bsuir.data.xml.FileProcessor;
import by.bsuir.data.xml.exception.XMLException;
import by.bsuir.util.global.Configurator;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class implements a pattern command
 * This class is necessary for luggage information generating
 * @author Michail Sychevsky
 */
public class ShowLuggageInformation implements Command {

    public static final Logger LOGGER = Logger.getLogger(ShowLuggageInformation.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            request.setAttribute(CommandConstants.ATTRIBUTE_LUGGAGE_PRICES, FileProcessor.getInstance().getPriceList().getLuggagePrices());
            request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_LUGGAGE_PAGE)).forward(request, response);
        }
        catch (XMLException e)
        {
            LOGGER.error(e);
            request.setAttribute(CommandConstants.ATTRIBUTE_EXCEPTION, XMLException.ERROR_ID);
            request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_SERVER_ERROR)).forward(request, response);
        }
    }
}
