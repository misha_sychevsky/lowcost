package by.bsuir.business.command.account;

import by.bsuir.business.command.Command;
import by.bsuir.business.service.CommandConstants;
import by.bsuir.data.database.dao.AbstractDAOFactory;
import by.bsuir.data.database.dao.framework.ClientDAO;
import by.bsuir.data.database.exception.DAOException;
import by.bsuir.data.database.exception.UnsupportedDatabaseException;
import by.bsuir.data.model.builder.ClientBuilder;
import by.bsuir.data.model.entity.Client;
import by.bsuir.util.global.Configurator;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class implements a pattern command
 * This class makes profile editing
 * @author Michail Sychevsky
 */
public class EditProfile implements Command {

    public static final Logger LOGGER = Logger.getLogger(EditProfile.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            Client client = (Client) request.getSession().getAttribute(CommandConstants.ATTRIBUTE_CLIENT);

            String firstName = request.getParameter(CommandConstants.PARAMETER_FIRST_NAME);
            String lastName = request.getParameter(CommandConstants.PARAMETER_LAST_NAME);
            String dateValue = request.getParameter(CommandConstants.PARAMETER_BIRTH_DATE);
            String phone = request.getParameter(CommandConstants.PARAMETER_PHONE);
            String eMail = client.getEmail();
            String login = client.getLogin();
            String password = request.getParameter(CommandConstants.PARAMETER_PASSWORD);
            String passport = request.getParameter(CommandConstants.PARAMETER_PASSPORT);
            String country = request.getParameter(CommandConstants.PARAMETER_COUNTRY);

            ClientBuilder builder = new ClientBuilder();
            if(builder.buildClient(firstName, lastName, dateValue, phone, eMail, login, password, passport, country))
            {
                Client updateClient = builder.getClient();
                updateClient.setId(client.getId());
                ClientDAO dao = AbstractDAOFactory.getDAOFactory(Configurator.getGlobalProperty(CommandConstants.USE_DATABASE)).getClientDAO();
                dao.updateClient(updateClient);

                request.setAttribute(CommandConstants.ATTRIBUTE_FLAG, CommandConstants.FLAG_EDIT_SUCCESSFUL);
                request.getSession().setAttribute(CommandConstants.ATTRIBUTE_CLIENT, updateClient);
                request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_EDIT_PAGE)).forward(request, response);
            }
            else
            {
                request.setAttribute(CommandConstants.ATTRIBUTE_FLAG, CommandConstants.FLAG_EDIT_FAILED);
                request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_EDIT_PAGE)).forward(request, response);
            }
        }
        catch (UnsupportedDatabaseException | DAOException e)
        {
            LOGGER.error(e);
            request.setAttribute(CommandConstants.ATTRIBUTE_EXCEPTION, DAOException.ERROR_ID);
            request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_SERVER_ERROR)).forward(request, response);
        }
    }
}
