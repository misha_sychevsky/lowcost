package by.bsuir.business.command.account;

import by.bsuir.business.command.Command;
import by.bsuir.business.service.CommandConstants;
import by.bsuir.data.database.dao.AbstractDAOFactory;
import by.bsuir.data.database.dao.framework.LuggageDAO;
import by.bsuir.data.database.dao.framework.TicketDAO;
import by.bsuir.data.database.exception.DAOException;
import by.bsuir.data.database.exception.UnsupportedDatabaseException;
import by.bsuir.data.model.entity.Client;
import by.bsuir.data.model.entity.Luggage;
import by.bsuir.data.model.entity.Ticket;
import by.bsuir.util.global.Configurator;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * This class implements a pattern command
 * This class is necessary for user home page information generating
 * @author Michail Sychevsky
 */
public class Home implements Command {

    public static final Logger LOGGER = Logger.getLogger(Home.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            Client client = (Client) request.getSession().getAttribute(CommandConstants.ATTRIBUTE_CLIENT);

            TicketDAO ticketDAO = AbstractDAOFactory.getDAOFactory(Configurator.getGlobalProperty(CommandConstants.USE_DATABASE)).getTicketDAO();
            LuggageDAO luggageDAO = AbstractDAOFactory.getDAOFactory(Configurator.getGlobalProperty(CommandConstants.USE_DATABASE)).getLuggageDAO();

            List<Ticket> tickets = ticketDAO.getTicketsPage(client, CommandConstants.FIRST_PAGE - 1, CommandConstants.TABLE_TICKETS_ITEMS_COUNT_ON_PAGE);
            List<Luggage> luggage = luggageDAO.getLuggagePage(client, CommandConstants.FIRST_PAGE - 1, CommandConstants.TABLE_LUGGAGE_ITEMS_COUNT_ON_PAGE);

            request.getSession().setAttribute(CommandConstants.ATTRIBUTE_TICKETS_PAGE, CommandConstants.FIRST_PAGE);
            request.getSession().setAttribute(CommandConstants.ATTRIBUTE_LUGGAGE_PAGE, CommandConstants.FIRST_PAGE);

            checkTicketsCount(tickets, request);
            checkLuggageCount(luggage, request);

            request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_CLIENT_PAGE)).forward(request, response);
        }
        catch (UnsupportedDatabaseException | DAOException e)
        {
            LOGGER.error(e);
            request.setAttribute(CommandConstants.ATTRIBUTE_EXCEPTION, DAOException.ERROR_ID);
            request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_SERVER_ERROR)).forward(request, response);
        }
    }

    /**
     * This method checks tickets count.
     *
     * Check the last ticket page or not or page is empty.
     * @param tickets
     * @param request
     */
    private void checkTicketsCount(List<Ticket> tickets, HttpServletRequest request)
    {
        if(tickets.size() != CommandConstants.TABLE_TICKETS_ITEMS_COUNT_ON_PAGE)
        {
            request.getSession().setAttribute(CommandConstants.ATTRIBUTE_LAST_TICKETS_PAGE, true);
        }
        else
        {
            request.getSession().setAttribute(CommandConstants.ATTRIBUTE_LAST_TICKETS_PAGE, false);
        }
        if(tickets.size() != CommandConstants.NO_ITEMS)
        {
            request.setAttribute(CommandConstants.ATTRIBUTE_TICKETS, tickets);
        }
        else
        {
            request.setAttribute(CommandConstants.ATTRIBUTE_TICKETS_FLAG, CommandConstants.FLAG_NO_TICKETS);
        }
    }

    /**
     * This method checks tickets count.
     *
     * Check the last luggage page or not or page is empty.
     * @param luggage
     * @param request
     */
    private void checkLuggageCount(List<Luggage> luggage, HttpServletRequest request)
    {
        if(luggage.size() != CommandConstants.TABLE_LUGGAGE_ITEMS_COUNT_ON_PAGE)
        {
            request.getSession().setAttribute(CommandConstants.ATTRIBUTE_LAST_LUGGAGE_PAGE, true);
        }
        else
        {
            request.getSession().setAttribute(CommandConstants.ATTRIBUTE_LAST_LUGGAGE_PAGE, false);
        }
        if(luggage.size() != CommandConstants.NO_ITEMS)
        {
            request.setAttribute(CommandConstants.ATTRIBUTE_LUGGAGE, luggage);
        }
        else
        {
            request.setAttribute(CommandConstants.ATTRIBUTE_LUGGAGE_FLAG, CommandConstants.FLAG_NO_LUGGAGE);
        }
    }
}
