package by.bsuir.business.command.account;

import by.bsuir.business.service.CommandConstants;
import by.bsuir.business.command.Command;
import by.bsuir.data.database.dao.AbstractDAOFactory;
import by.bsuir.data.database.dao.framework.ClientDAO;
import by.bsuir.data.database.exception.DAOException;
import by.bsuir.data.database.exception.UnsupportedDatabaseException;
import by.bsuir.data.model.builder.ClientBuilder;
import by.bsuir.data.model.entity.Client;
import by.bsuir.util.global.Configurator;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class implements a pattern command
 * This class is necessary for user registration
 * @author Michail Sychevsky
 */
public class Registration implements Command {

    public static final Logger LOGGER = Logger.getLogger(Registration.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            String firstName = request.getParameter(CommandConstants.PARAMETER_FIRST_NAME);
            String lastName = request.getParameter(CommandConstants.PARAMETER_LAST_NAME);
            String dateValue = request.getParameter(CommandConstants.PARAMETER_BIRTH_DATE);
            String phone = request.getParameter(CommandConstants.PARAMETER_PHONE);
            String eMail = request.getParameter(CommandConstants.PARAMETER_E_MAIL);
            String login = request.getParameter(CommandConstants.PARAMETER_LOGIN);
            String password = request.getParameter(CommandConstants.PARAMETER_PASSWORD);
            String passport = request.getParameter(CommandConstants.PARAMETER_PASSPORT);
            String country = request.getParameter(CommandConstants.PARAMETER_COUNTRY);

            ClientBuilder builder = new ClientBuilder();
            if(builder.buildClient(firstName, lastName, dateValue, phone, eMail, login, password, passport, country))
            {
                Client client = builder.getClient();
                ClientDAO dao = AbstractDAOFactory.getDAOFactory(Configurator.getGlobalProperty(CommandConstants.USE_DATABASE)).getClientDAO();
                if(!dao.isClientExists(client))
                {
                    dao.insertClient(client);
                    request.setAttribute(CommandConstants.ATTRIBUTE_FLAG, CommandConstants.FLAG_REGISTRATION_SUCCESSFUL);
                    request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_START_PAGE)).forward(request, response);
                }
                else
                {
                    request.setAttribute(CommandConstants.ATTRIBUTE_FLAG, CommandConstants.FLAG_SUCH_LOGIN_EXISTS);
                    request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_REGISTRATION_PAGE)).forward(request, response);
                }
            }
            else
            {
                request.setAttribute(CommandConstants.ATTRIBUTE_FLAG, CommandConstants.FLAG_REGISTRATION_FAILED);
                request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_REGISTRATION_PAGE)).forward(request, response);
            }
        }
        catch (UnsupportedDatabaseException | DAOException e)
        {
            LOGGER.error(e);
            request.setAttribute(CommandConstants.ATTRIBUTE_EXCEPTION, DAOException.ERROR_ID);
            request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_SERVER_ERROR)).forward(request, response);
        }
    }
}
