package by.bsuir.business.command.account;

import by.bsuir.business.service.CommandConstants;
import by.bsuir.business.command.Command;
import by.bsuir.util.global.Configurator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class implements a pattern command
 * This class is necessary for user exit from profile
 * @author Michail Sychevsky
 */
public class Exit implements Command {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        request.getSession().invalidate();
        request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_START_PAGE)).forward(request,response);
    }
}
