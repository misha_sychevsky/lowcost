package by.bsuir.business.command.account;

import by.bsuir.business.service.CommandConstants;
import by.bsuir.business.command.Command;
import by.bsuir.data.database.dao.AbstractDAOFactory;
import by.bsuir.data.database.dao.framework.ClientDAO;
import by.bsuir.data.database.exception.DAOException;
import by.bsuir.data.database.exception.UnsupportedDatabaseException;
import by.bsuir.data.model.entity.Client;
import by.bsuir.util.global.Configurator;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class implements a pattern command
 * This class authorizes users
 * @author Michail Sychevsky
 */
public class Authorization implements Command {

    public static final Logger LOGGER = Logger.getLogger(Authorization.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            String login = request.getParameter(CommandConstants.PARAMETER_LOGIN);
            String password = request.getParameter(CommandConstants.PARAMETER_PASSWORD);

            ClientDAO dao = AbstractDAOFactory.getDAOFactory(Configurator.getGlobalProperty(CommandConstants.USE_DATABASE)).getClientDAO();
            Client client = dao.getClientByAuthDate(login, password);
            if(client != null)
            {
                request.getSession().setAttribute(CommandConstants.ATTRIBUTE_CLIENT, client);
                request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_START_PAGE)).forward(request, response);
            }
            else
            {
                request.setAttribute(CommandConstants.ATTRIBUTE_FLAG, CommandConstants.FLAG_AUTHORIZATION_FAILED);
                request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_START_PAGE)).forward(request, response);
            }
        }
        catch (UnsupportedDatabaseException | DAOException e)
        {
            LOGGER.error(e);
            request.setAttribute(CommandConstants.ATTRIBUTE_EXCEPTION, DAOException.ERROR_ID);
            request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_SERVER_ERROR)).forward(request, response);
        }
    }
}
