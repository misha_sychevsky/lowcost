package by.bsuir.business.command.settings;

import by.bsuir.business.command.Command;
import by.bsuir.business.service.CommandConstants;
import by.bsuir.util.global.Configurator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class implements a pattern command
 * This class set the interface language of web application.
 * @author Michail Sychevsky
 */
public class ChangeLanguage implements Command {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String language = request.getParameter(CommandConstants.PARAMETER_LANGUAGE);

        request.getSession().setAttribute(CommandConstants.PARAMETER_LANGUAGE, language);
        request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_START_PAGE)).forward(request, response);
    }
}
