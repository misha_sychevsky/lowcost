package by.bsuir.business.command.navigation.table.strategy.next;

import by.bsuir.business.command.navigation.table.strategy.Printing;
import by.bsuir.business.service.CommandConstants;
import by.bsuir.data.database.dao.AbstractDAOFactory;
import by.bsuir.data.database.dao.framework.LuggageDAO;
import by.bsuir.data.database.exception.DAOException;
import by.bsuir.data.database.exception.UnsupportedDatabaseException;
import by.bsuir.data.model.entity.Client;
import by.bsuir.data.model.entity.Luggage;
import by.bsuir.util.global.Configurator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * This class implements a pattern command
 * This class prints next luggage page
 * @author Michail Sychevsky
 */
public class PrintNextLuggagePage implements Printing {

    @Override
    public void print(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            HttpSession session = request.getSession();
            Client client = (Client) session.getAttribute(CommandConstants.ATTRIBUTE_CLIENT);

            if(session.getAttribute(CommandConstants.ATTRIBUTE_LAST_LUGGAGE_PAGE) != true)
            {
                int page = (Integer) session.getAttribute(CommandConstants.ATTRIBUTE_LUGGAGE_PAGE);
                int start = page * CommandConstants.TABLE_LUGGAGE_ITEMS_COUNT_ON_PAGE;
                LuggageDAO dao = AbstractDAOFactory.getDAOFactory(Configurator.getGlobalProperty(CommandConstants.USE_DATABASE)).getLuggageDAO();
                List<Luggage> luggage = dao.getLuggagePage(client, start, CommandConstants.TABLE_LUGGAGE_ITEMS_COUNT_ON_PAGE);
                if(luggage.size() != CommandConstants.TABLE_LUGGAGE_ITEMS_COUNT_ON_PAGE)
                {
                    session.setAttribute(CommandConstants.ATTRIBUTE_LAST_LUGGAGE_PAGE, true);
                }
                if(luggage.size() != CommandConstants.NO_ITEMS)
                {
                    session.setAttribute(CommandConstants.ATTRIBUTE_LUGGAGE_PAGE, ++page);
                    request.setAttribute(CommandConstants.ATTRIBUTE_LUGGAGE, luggage);
                    request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_PRINT_LUGGAGE_PAGE)).forward(request, response);
                }
                else
                {
                    session.setAttribute(CommandConstants.ATTRIBUTE_LAST_LUGGAGE_PAGE, true);
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                }
            }
            else
            {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        }
        catch (UnsupportedDatabaseException | DAOException e)
        {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
