package by.bsuir.business.command.navigation.global;

import by.bsuir.business.service.CommandConstants;
import by.bsuir.business.command.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class implements a pattern command
 * This class is necessary to forward to another page.
 * @author Michail Sychevsky
 */
public class ForwardToPage implements Command {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String forward = request.getParameter(CommandConstants.PARAMETER_FORWARD_PAGE);
        if (forward != null)
        {
            request.getRequestDispatcher(forward).forward(request, response);
        }
        else
        {
            request.getRequestDispatcher(CommandConstants.FORWARD_START_PAGE).forward(request, response);
        }
    }
}
