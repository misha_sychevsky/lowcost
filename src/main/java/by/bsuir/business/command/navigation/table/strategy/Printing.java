package by.bsuir.business.command.navigation.table.strategy;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Strategy pattern.
 * Printing of pages.
 * @author Michail Sychevsky
 */
public interface Printing {

    void print(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
}
