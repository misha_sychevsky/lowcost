package by.bsuir.business.command.navigation.table;

import by.bsuir.business.command.Command;
import by.bsuir.business.command.navigation.table.strategy.PagePrinter;
import by.bsuir.business.service.CommandConstants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class implements a pattern command
 * This class is necessary for printing the next or preview page of table from request
 * @author Michail Sychevsky
 */
public class PrintPage implements Command {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String pageType = request.getParameter(CommandConstants.PARAMETER_PAGE);
        String navigation = request.getParameter(CommandConstants.PARAMETER_NAVIGATION);

        PagePrinter printer = new PagePrinter(pageType, navigation);
        printer.printPage(request, response);
    }
}
