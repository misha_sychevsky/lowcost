package by.bsuir.business.command.navigation.table.strategy.next;

import by.bsuir.business.command.navigation.table.strategy.Printing;
import by.bsuir.business.service.CommandConstants;
import by.bsuir.data.database.dao.AbstractDAOFactory;
import by.bsuir.data.database.dao.framework.TicketPriceDAO;
import by.bsuir.data.database.exception.DAOException;
import by.bsuir.data.database.exception.UnsupportedDatabaseException;
import by.bsuir.data.model.entity.TicketPrice;
import by.bsuir.data.model.type.ClassType;
import by.bsuir.util.global.Configurator;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.util.List;


/**
 * This class implements a pattern command
 * This class prints next flights page
 * @author Michail Sychevsky
 */
public class PrintNextFlightsPage implements Printing {

    public static final Logger LOGGER = Logger.getLogger(PrintNextFlightsPage.class);

    @Override
    public void print(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            HttpSession session = request.getSession();
            String from = (String) session.getAttribute(CommandConstants.PARAMETER_FROM_CITY);
            String to = (String) session.getAttribute(CommandConstants.PARAMETER_TO_CITY);
            Date date = (Date) session.getAttribute(CommandConstants.PARAMETER_DATE);
            ClassType classType = (ClassType) session.getAttribute(CommandConstants.PARAMETER_CLASS);

            if(session.getAttribute(CommandConstants.ATTRIBUTE_LAST_FLIGHTS_PAGE) != true)
            {
                int page = (Integer) session.getAttribute(CommandConstants.ATTRIBUTE_FLIGHTS_PAGE);
                int start = page * CommandConstants.TABLE_FLIGHTS_ITEMS_COUNT_ON_PAGE;
                TicketPriceDAO dao = AbstractDAOFactory.getDAOFactory(Configurator.getGlobalProperty(CommandConstants.USE_DATABASE)).getTicketPriceDAO();
                List<TicketPrice> ticketPrices = dao.getTicketPricesPageBySearchParameters(from, to, date, classType, start, CommandConstants.TABLE_FLIGHTS_ITEMS_COUNT_ON_PAGE);
                if(ticketPrices.size() != CommandConstants.TABLE_FLIGHTS_ITEMS_COUNT_ON_PAGE)
                {
                    session.setAttribute(CommandConstants.ATTRIBUTE_LAST_FLIGHTS_PAGE, true);
                }
                if(ticketPrices.size() != CommandConstants.NO_ITEMS)
                {
                    session.setAttribute(CommandConstants.ATTRIBUTE_FLIGHTS_PAGE, ++page);
                    request.setAttribute(CommandConstants.ATTRIBUTE_TICKET_PRICES, ticketPrices);
                    request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_PRINT_FLIGHT_PAGE)).forward(request, response);
                }
                else
                {
                    session.setAttribute(CommandConstants.ATTRIBUTE_LAST_FLIGHTS_PAGE, true);
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                }
            }
            else
            {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        }
        catch (UnsupportedDatabaseException | DAOException e)
        {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
