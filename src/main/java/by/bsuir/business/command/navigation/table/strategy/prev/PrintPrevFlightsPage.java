package by.bsuir.business.command.navigation.table.strategy.prev;

import by.bsuir.business.command.navigation.table.strategy.Printing;
import by.bsuir.business.service.CommandConstants;
import by.bsuir.data.database.dao.AbstractDAOFactory;
import by.bsuir.data.database.dao.framework.TicketPriceDAO;
import by.bsuir.data.database.exception.DAOException;
import by.bsuir.data.database.exception.UnsupportedDatabaseException;
import by.bsuir.data.model.entity.TicketPrice;
import by.bsuir.data.model.type.ClassType;
import by.bsuir.util.global.Configurator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.util.List;

/**
 * This class implements a pattern command
 * This class prints preview flights page
 * @author Michail Sychevsky
 */
public class PrintPrevFlightsPage implements Printing {

    @Override
    public void print(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            HttpSession session = request.getSession();
            String from = (String) session.getAttribute(CommandConstants.PARAMETER_FROM_CITY);
            String to = (String) session.getAttribute(CommandConstants.PARAMETER_TO_CITY);
            Date date = (Date) session.getAttribute(CommandConstants.PARAMETER_DATE);
            ClassType classType = (ClassType) session.getAttribute(CommandConstants.PARAMETER_CLASS);

            int page = (Integer) session.getAttribute(CommandConstants.ATTRIBUTE_FLIGHTS_PAGE);
            if(page != CommandConstants.FIRST_PAGE)
            {
                int start = (page == 2 ? 0 : (page - 2) * CommandConstants.TABLE_FLIGHTS_ITEMS_COUNT_ON_PAGE - 1);
                TicketPriceDAO dao = AbstractDAOFactory.getDAOFactory(Configurator.getGlobalProperty(CommandConstants.USE_DATABASE)).getTicketPriceDAO();
                List<TicketPrice> ticketPrices = dao.getTicketPricesPageBySearchParameters(from, to, date, classType, start, CommandConstants.TABLE_FLIGHTS_ITEMS_COUNT_ON_PAGE);

                if(session.getAttribute(CommandConstants.ATTRIBUTE_LAST_FLIGHTS_PAGE) == true)
                {
                    session.setAttribute(CommandConstants.ATTRIBUTE_LAST_FLIGHTS_PAGE, false);
                }
                session.setAttribute(CommandConstants.ATTRIBUTE_FLIGHTS_PAGE, --page);
                request.setAttribute(CommandConstants.ATTRIBUTE_TICKET_PRICES, ticketPrices);
                request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_PRINT_FLIGHT_PAGE)).forward(request, response);
            }
            else
            {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        }
        catch (UnsupportedDatabaseException | DAOException e)
        {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
