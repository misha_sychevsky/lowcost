package by.bsuir.business.command.navigation.table.strategy;

import by.bsuir.business.command.navigation.table.strategy.next.PrintNextFlightsPage;
import by.bsuir.business.command.navigation.table.strategy.next.PrintNextLuggagePage;
import by.bsuir.business.command.navigation.table.strategy.next.PrintNextTicketsPage;
import by.bsuir.business.command.navigation.table.strategy.prev.PrintPrevFlightsPage;
import by.bsuir.business.command.navigation.table.strategy.prev.PrintPrevLuggagePage;
import by.bsuir.business.command.navigation.table.strategy.prev.PrintPrevTicketsPage;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class implements a pattern strategy
 * This class defines which page and from which table to print
 * @author Michail Sychevsky
 */
public class PagePrinter {

    public static final String PREV_PAGE = "prev";
    public static final String NEXT_PAGE = "next";

    public static final String PAGE_TYPE_TICKETS = "tickets_page";
    public static final String PAGE_TYPE_LUGGAGE = "luggage_page";
    public static final String PAGE_TYPE_FLIGHTS = "flights_page";

    private Printing printing;

    public PagePrinter(String pageType, String navigation)
    {
        if(navigation.equals(PREV_PAGE))
        {
            switch(pageType)
            {
                case PAGE_TYPE_TICKETS:
                    this.printing = new PrintPrevTicketsPage();
                    break;

                case PAGE_TYPE_LUGGAGE:
                    this.printing = new PrintPrevLuggagePage();
                    break;

                case PAGE_TYPE_FLIGHTS:
                    this.printing = new PrintPrevFlightsPage();
                    break;
            }
        }

        if(navigation.equals(NEXT_PAGE))
        {
            switch ((pageType))
            {
                case PAGE_TYPE_TICKETS:
                    this.printing = new PrintNextTicketsPage();
                    break;

                case PAGE_TYPE_LUGGAGE:
                    this.printing = new PrintNextLuggagePage();
                    break;

                case PAGE_TYPE_FLIGHTS:
                    this.printing = new PrintNextFlightsPage();
                    break;
            }
        }
    }

    /**
     * Print defined page.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void printPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        this.printing.print(request, response);
    }
}
