package by.bsuir.business.command.navigation.table.strategy.next;

import by.bsuir.business.command.navigation.table.strategy.Printing;
import by.bsuir.business.service.CommandConstants;
import by.bsuir.data.database.dao.AbstractDAOFactory;
import by.bsuir.data.database.dao.framework.TicketDAO;
import by.bsuir.data.database.exception.DAOException;
import by.bsuir.data.database.exception.UnsupportedDatabaseException;
import by.bsuir.data.model.entity.Client;
import by.bsuir.data.model.entity.Ticket;
import by.bsuir.util.global.Configurator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * This class implements a pattern command
 * This class prints next ticket page
 * @author Michail Sychevsky
 */
public class PrintNextTicketsPage implements Printing {

    @Override
    public void print(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            HttpSession session = request.getSession();
            Client client = (Client) session.getAttribute(CommandConstants.ATTRIBUTE_CLIENT);

            if(session.getAttribute(CommandConstants.ATTRIBUTE_LAST_TICKETS_PAGE) != true)
            {
                int page = (Integer) session.getAttribute(CommandConstants.ATTRIBUTE_TICKETS_PAGE);
                int start = page * CommandConstants.TABLE_TICKETS_ITEMS_COUNT_ON_PAGE;
                TicketDAO dao = AbstractDAOFactory.getDAOFactory(Configurator.getGlobalProperty(CommandConstants.USE_DATABASE)).getTicketDAO();
                List<Ticket> tickets = dao.getTicketsPage(client, start, CommandConstants.TABLE_TICKETS_ITEMS_COUNT_ON_PAGE);
                if(tickets.size() != CommandConstants.TABLE_TICKETS_ITEMS_COUNT_ON_PAGE)
                {
                    session.setAttribute(CommandConstants.ATTRIBUTE_LAST_TICKETS_PAGE, true);
                }
                if(tickets.size() != CommandConstants.NO_ITEMS)
                {
                    session.setAttribute(CommandConstants.ATTRIBUTE_TICKETS_PAGE, ++page);
                    request.setAttribute(CommandConstants.ATTRIBUTE_TICKETS, tickets);
                    request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_PRINT_TICKET_PAGE)).forward(request, response);
                }
                else
                {
                    session.setAttribute(CommandConstants.ATTRIBUTE_LAST_TICKETS_PAGE, true);
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                }
            }
            else
            {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        }
        catch (UnsupportedDatabaseException | DAOException e)
        {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
