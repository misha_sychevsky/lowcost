package by.bsuir.business.exception;

/**
 * Exception no such command.
 * @author Michail Sychevsky
 */
public class NoSuchCommandException extends Exception {

    public static final String NO_SUCH_COMMAND_EXCEPTION_MESSAGE = "No such command!";

    public NoSuchCommandException()
    {
        super(NO_SUCH_COMMAND_EXCEPTION_MESSAGE);
    }
}
