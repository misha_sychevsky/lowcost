package by.bsuir.data.model.builder;

import by.bsuir.data.model.entity.Client;
import by.bsuir.util.global.Configurator;

import java.sql.Date;

/**
 * This class check all parameters and build the client object.
 * @author Michail Sychevsky
 */
public class ClientBuilder {

    public static final String REGEXP_PHONE = "regexp_phone";
    public static final String REGEXP_E_MAIL = "regexp_email";
    public static final String REGEXP_LOGIN= "regexp_login";
    public static final String REGEXP_PASSWORD = "regexp_password";
    public static final String REGEXP_PASSPORT = "regexp_passport";
    public static final String REGEXP_TEXT_FIELD = "regexp_text_field";

    private Client client = new Client();

    public Client getClient()
    {
        return client;
    }

    public boolean buildClient(String firstName, String lastName, String birthDate,
                               String phone, String eMail, String login,
                               String password, String passport, String country)
    {
        boolean result = true;
        result &= checkFirstName(firstName);
        result &= checkLastName(lastName);
        result &= checkPhone(phone);
        result &= checkEMail(eMail);
        result &= checkLogin(login);
        result &= checkPassword(password);
        result &= checkPassport(passport);
        result &= checkCountry(country);
        result &= checkDate(birthDate);
        return result;
    }

    private boolean checkFirstName(String firstName)
    {
        if(firstName.matches(Configurator.getGlobalProperty(REGEXP_TEXT_FIELD)))
        {
            client.setFirstName(firstName);
            return true;
        }
        return false;
    }

    private boolean checkLastName(String lastName)
    {
        if(lastName.matches(Configurator.getGlobalProperty(REGEXP_TEXT_FIELD)))
        {
            client.setLastName(lastName);
            return true;
        }
        return false;
    }

    private boolean checkDate(String dateValue)
    {
        try
        {
            Date date = Date.valueOf(dateValue);
            client.setBirthDate(date);
            return true;
        }
        catch (IllegalArgumentException e)
        {
            return false;
        }
    }

    private boolean checkPhone(String phone)
    {
        if(phone.matches(Configurator.getGlobalProperty(REGEXP_PHONE)))
        {
            client.setPhone(phone);
            return true;
        }
        return false;
    }

    private boolean checkEMail(String eMail)
    {
        if(eMail.matches(Configurator.getGlobalProperty(REGEXP_E_MAIL)))
        {
            client.setEmail(eMail);
            return true;
        }
        return false;
    }
    private boolean checkPassword(String password)
    {
        if(password.matches(Configurator.getGlobalProperty(REGEXP_PASSWORD)))
        {
            client.setPassword(password);
            return true;
        }
        return false;
    }

    private boolean checkLogin(String login)
    {
        if(login.matches(Configurator.getGlobalProperty(REGEXP_LOGIN)))
        {
            client.setLogin(login);
            return true;
        }
        return false;
    }

    private boolean checkPassport(String passport)
    {
        if(passport.matches(Configurator.getGlobalProperty(REGEXP_PASSPORT)))
        {
            client.setPassportNS(passport);
            return true;
        }
        return false;
    }

    private boolean checkCountry(String country)
    {
        if(country.matches(Configurator.getGlobalProperty(REGEXP_TEXT_FIELD)))
        {
            client.setCountry(country);
            return true;
        }
        return false;
    }
}
