package by.bsuir.data.model.builder;

import by.bsuir.data.model.entity.LuggagePrice;

/**
 * This class build the luggage price immutable object.
 * @author Michail Sychevsky
 */
public class LuggagePriceBuilder {

    private double minVolume;
    private double maxVolume;
    private double minWeight;
    private double maxWeight;
    private double price;

    public void setMinVolume(double minVolume)
    {
        this.minVolume = minVolume;
    }

    public void setMaxVolume(double maxVolume)
    {
        this.maxVolume = maxVolume;
    }

    public void setMinWeight(double minWeight)
    {
        this.minWeight = minWeight;
    }

    public void setMaxWeight(double maxWeight)
    {
        this.maxWeight = maxWeight;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public LuggagePrice build()
    {
        return new LuggagePrice(minVolume, maxVolume,
                minWeight, maxWeight, price);
    }
}
