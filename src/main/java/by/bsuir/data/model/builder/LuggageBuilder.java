package by.bsuir.data.model.builder;

import by.bsuir.data.model.entity.Client;
import by.bsuir.data.model.entity.Flight;
import by.bsuir.data.model.entity.Luggage;
import by.bsuir.util.global.Configurator;

/**
 * This class check all parameters and build the luggage object.
 *
 * @author Michail Sychevsky
 */
public class LuggageBuilder {

    public static final String REGEXP_DOUBLE = "regexp_double";
    public static final String MAX_WEIGHT = "luggage.max.weight";
    public static final String MAX_VOLUME = "luggage.max.volume";

    private Luggage luggage = new Luggage();

    public Luggage getLuggage()
    {
        return luggage;
    }

    public boolean buildLuggage(String weight, String volume, Client client,
                                Flight flight)
    {
        boolean result = true;
        result &= checkWeight(weight);
        result &= checkVolume(volume);
        luggage.setClient(client);
        luggage.setFlight(flight);
        return result;
    }

    private boolean checkWeight(String weight)
    {
        boolean result = false;
        if(weight.matches(Configurator.getGlobalProperty(REGEXP_DOUBLE)))
        {
            double weightValue = Double.parseDouble(weight);
            if(weightValue <= Double.parseDouble(Configurator.getGlobalProperty(MAX_WEIGHT)))
            {
                luggage.setWeight(weightValue);
                result = true;
            }
        }
        return result;
    }

    private boolean checkVolume(String volume)
    {
        boolean result = false;
        if(volume.matches(Configurator.getGlobalProperty(REGEXP_DOUBLE)))
        {
            double volumeValue = Double.parseDouble(volume);
            if(volumeValue <= Double.parseDouble(Configurator.getGlobalProperty(MAX_VOLUME)))
            {
                luggage.setVolume(volumeValue);
                result = true;
            }
        }
        return result;
    }
}
