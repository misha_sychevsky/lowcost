package by.bsuir.data.model.entity;

import java.io.Serializable;

/**
 * Luggage price entity
 *
 * @author Michail Sychevsky
 */
public class LuggagePrice implements Serializable {

    private double minVolume;
    private double maxVolume;
    private double minWeight;
    private double maxWeight;
    private double price;

    public LuggagePrice(double minVolume, double maxVolume,
                        double minWeight, double maxWeight,
                        double price)
    {
        this.minVolume = minVolume;
        this.maxVolume = maxVolume;
        this.minWeight = minWeight;
        this.maxWeight = maxWeight;
        this.price = price;
    }

    public double getMinVolume()
    {
        return minVolume;
    }

    public double getMaxVolume()
    {
        return maxVolume;
    }

    public double getPrice()
    {
        return price;
    }

    public double getMaxWeight()
    {
        return maxWeight;
    }

    public double getMinWeight()
    {
        return minWeight;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        LuggagePrice that = (LuggagePrice) o;

        if (Double.compare(that.maxVolume, maxVolume) != 0)
        {
            return false;
        }
        if (Double.compare(that.maxWeight, maxWeight) != 0)
        {
            return false;
        }
        if (Double.compare(that.minVolume, minVolume) != 0)
        {
            return false;
        }
        if (Double.compare(that.minWeight, minWeight) != 0)
        {
            return false;
        }
        if (Double.compare(that.price, price) != 0)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = 17;
        long temp;
        temp = Double.doubleToLongBits(minVolume);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(maxVolume);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(minWeight);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(maxWeight);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString()
    {
        StringBuilder result = new StringBuilder("LuggagePrice: ");
        result.append("minVolume=")
                .append(minVolume)
                .append(", maxVolume=")
                .append(maxVolume)
                .append(", minWeight=")
                .append(minWeight)
                .append(", maxWeight=")
                .append(maxWeight)
                .append(", price=")
                .append(price);
        return result.toString();
    }
}
