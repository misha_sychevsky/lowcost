package by.bsuir.data.model.entity;

import java.io.Serializable;

/**
 * Luggage entity
 * @author Michail Sychevsky
 */
public class Luggage  implements Serializable {

    private int id;
    private double weight;
    private double volume;
    private Client client;
    private Flight flight;
    private double price;

    public double getPrice()
    {
        return price;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public Flight getFlight()
    {
        return flight;
    }

    public void setFlight(Flight flight)
    {
        this.flight = flight;
    }

    public Client getClient()
    {
        return client;
    }

    public void setClient(Client client)
    {
        this.client = client;
    }

    public double getVolume()
    {
        return volume;
    }

    public void setVolume(double volume)
    {
        this.volume = volume;
    }

    public double getWeight()
    {
        return weight;
    }

    public void setWeight(double weight)
    {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Luggage luggage = (Luggage) o;

        if (id != luggage.id)
        {
            return false;
        }
        if (Double.compare(luggage.price, price) != 0)
        {
            return false;
        }
        if (Double.compare(luggage.volume, volume) != 0)
        {
            return false;
        }
        if (Double.compare(luggage.weight, weight) != 0)
        {
            return false;
        }
        if (client != null ? !client.equals(luggage.client) : luggage.client != null)
        {
            return false;
        }
        if (flight != null ? !flight.equals(luggage.flight) : luggage.flight != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = 17;
        long temp;
        result = id;
        temp = Double.doubleToLongBits(weight);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(volume);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (client != null ? client.hashCode() : 0);
        result = 31 * result + (flight != null ? flight.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString()
    {
        StringBuilder result = new StringBuilder("Flight: ");
        result.append("id=")
                .append(id)
                .append(", weight=")
                .append(weight)
                .append(", volume=")
                .append(volume)
                .append(", clientId=")
                .append(client.getId())
                .append(", flightId=")
                .append(flight.getId())
                .append(", price=")
                .append(price);
        return result.toString();
    }
}
