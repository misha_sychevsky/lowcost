package by.bsuir.data.model.entity;

import java.io.Serializable;
import java.sql.Date;

/**
 * Client entity
 * @author Michail Sychevsky
 */
public class Client implements Serializable{

    private int id;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String phone;
    private String email;
    private String login;
    private String password;
    private String passportNS;
    private String country;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public Date getBirthDate()
    {
        return birthDate;
    }

    public void setBirthDate(Date birthDate)
    {
        this.birthDate = birthDate;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassportNS()
    {
        return passportNS;
    }

    public void setPassportNS(String passportNS)
    {
        this.passportNS = passportNS;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Client client = (Client) o;

        if (id != client.id)
        {
            return false;
        }
        if (!country.equals(client.country))
        {
            return false;
        }
        if (birthDate != null ? !birthDate.equals(client.birthDate) : client.birthDate != null)
        {
            return false;
        }
        if (!email.equals(client.email))
        {
            return false;
        }
        if (!firstName.equals(client.firstName))
        {
            return false;
        }
        if (!lastName.equals(client.lastName))
        {
            return false;
        }
        if (!login.equals(client.login))
        {
            return false;
        }
        if (!passportNS.equals(client.passportNS))
        {
            return false;
        }
        if (!password.equals(client.password))
        {
            return false;
        }
        if (phone != null ? !phone.equals(client.phone) : client.phone != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = 17;
        result = 31 * result + id;
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + email.hashCode();
        result = 31 * result + login.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + passportNS.hashCode();
        result = 31 * result + country.hashCode();
        return result;
    }

    @Override
    public String toString()
    {
        StringBuilder result = new StringBuilder("Client: ");
        result.append("id=")
                .append(id)
                .append(", firstName=")
                .append(firstName)
                .append(", lastName=")
                .append(lastName)
                .append(", birthDate=")
                .append(birthDate)
                .append(", phone=")
                .append(phone)
                .append(", email=")
                .append(email)
                .append(", login=")
                .append(login)
                .append(", password=")
                .append(password)
                .append(", passportNS=")
                .append(passportNS)
                .append(", country=")
                .append(country);
        return result.toString();
    }
}
