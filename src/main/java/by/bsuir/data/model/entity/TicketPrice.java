package by.bsuir.data.model.entity;

import by.bsuir.data.model.type.ClassType;

import java.io.Serializable;

/**
 * Ticket price entity
 * @author Michail Sychevsky
 */
public class TicketPrice implements Serializable {

    private Flight flight;
    private ClassType classType;
    private int seatsCount;
    private int freeSeatsCount;
    private double price;
    private double privilegeRBPrice;

    public Flight getFlight()
    {
        return flight;
    }

    public void setFlight(Flight flight)
    {
        this.flight = flight;
    }

    public double getPrivilegeRBPrice()
    {
        return privilegeRBPrice;
    }

    public void setPrivilegeRBPrice(double privilegeRBPrice)
    {
        this.privilegeRBPrice = privilegeRBPrice;
    }

    public double getPrice()
    {
        return price;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public int getFreeSeatsCount()
    {
        return freeSeatsCount;
    }

    public void setFreeSeatsCount(int freeSeatsCount)
    {
        this.freeSeatsCount = freeSeatsCount;
    }

    public ClassType getClassType()
    {
        return classType;
    }

    public void setClassType(ClassType classType)
    {
        this.classType = classType;
    }

    public int getSeatsCount()
    {
        return seatsCount;
    }

    public void setSeatsCount(int seatsCount)
    {
        this.seatsCount = seatsCount;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        TicketPrice that = (TicketPrice) o;

        if (freeSeatsCount != that.freeSeatsCount)
        {
            return false;
        }
        if (Double.compare(that.price, price) != 0)
        {
            return false;
        }
        if (Double.compare(that.privilegeRBPrice, privilegeRBPrice) != 0)
        {
            return false;
        }
        if (seatsCount != that.seatsCount)
        {
            return false;
        }
        if (classType != that.classType)
        {
            return false;
        }
        if (!flight.equals(that.flight))
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = 17;
        long temp;
        result = flight.hashCode();
        result = 31 * result + classType.hashCode();
        result = 31 * result + seatsCount;
        result = 31 * result + freeSeatsCount;
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(privilegeRBPrice);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString()
    {
        StringBuilder result = new StringBuilder("TicketPrice: ");
        result.append("flightId=")
                .append(flight.getId())
                .append(", classType=")
                .append(classType.value())
                .append(", seatsCount=")
                .append(seatsCount)
                .append(", freeSeatsCount=")
                .append(freeSeatsCount)
                .append(", price=")
                .append(price)
                .append(", privilegeRBPrice=")
                .append(privilegeRBPrice);
        return result.toString();
    }
}
