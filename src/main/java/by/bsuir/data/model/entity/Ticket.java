package by.bsuir.data.model.entity;

import by.bsuir.data.model.type.ClassType;

import java.io.Serializable;

/**
 * Ticket entity
 * @author Michail Sychevsky
 */
public class Ticket implements Serializable {

    private String seat;
    private boolean privilegeRB;
    private ClassType classType;
    private double price;
    private Flight flight;
    private Client client;

    public String getSeat()
    {
        return seat;
    }

    public void setSeat(String seat)
    {
        this.seat = seat;
    }

    public Client getClient()
    {
        return client;
    }

    public void setClient(Client client)
    {
        this.client = client;
    }

    public Flight getFlight()
    {
        return flight;
    }

    public void setFlight(Flight flight)
    {
        this.flight = flight;
    }

    public double getPrice()
    {
        return price;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public ClassType getClassType()
    {
        return classType;
    }

    public void setClassType(ClassType classType)
    {
        this.classType = classType;
    }

    public boolean isPrivilegeRB()
    {
        return privilegeRB;
    }

    public void setPrivilegeRB(boolean privilegeRB)
    {
        this.privilegeRB = privilegeRB;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Ticket ticket = (Ticket) o;

        if (Double.compare(ticket.price, price) != 0)
        {
            return false;
        }
        if (privilegeRB != ticket.privilegeRB)
        {
            return false;
        }
        if (classType != ticket.classType)
        {
            return false;
        }
        if (client != null ? !client.equals(ticket.client) : ticket.client != null)
        {
            return false;
        }
        if (flight != null ? !flight.equals(ticket.flight) : ticket.flight != null)
        {
            return false;
        }
        if (seat != null ? !seat.equals(ticket.seat) : ticket.seat != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = 17;
        long temp;
        result = seat != null ? seat.hashCode() : 0;
        result = 31 * result + (privilegeRB ? 1 : 0);
        result = 31 * result + (classType != null ? classType.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (flight != null ? flight.hashCode() : 0);
        result = 31 * result + (client != null ? client.hashCode() : 0);
        return result;
    }

    @Override
    public String toString()
    {
        StringBuilder result = new StringBuilder("Ticket: ");
        result.append("seat=")
                .append(seat)
                .append(", privilegeRB=")
                .append(privilegeRB)
                .append(", classType=")
                .append(classType.value())
                .append(", price=")
                .append(price)
                .append(", flightId=")
                .append(flight.getId())
                .append(", clientId=")
                .append(client.getId());
        return result.toString();
    }
}
