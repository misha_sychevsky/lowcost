package by.bsuir.data.model.entity;

import java.io.Serializable;
import java.sql.Date;

/**
 * Flight entity
 * @author Michail Sychevsky
 */
public class Flight implements Serializable {

    private int id;
    private String from;
    private String to;
    private Date date;
    private String departureTime;
    private String arrivalTime;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getArrivalTime()
    {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime)
    {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureTime()
    {
        return departureTime;
    }

    public void setDepartureTime(String departureTime)
    {
        this.departureTime = departureTime;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public String getFrom()
    {
        return from;
    }

    public void setFrom(String from)
    {
        this.from = from;
    }

    public String getTo()
    {
        return to;
    }

    public void setTo(String to)
    {
        this.to = to;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Flight flight = (Flight) o;

        if (id != flight.id)
        {
            return false;
        }
        if (!arrivalTime.equals(flight.arrivalTime))
        {
            return false;
        }
        if (!date.equals(flight.date))
        {
            return false;
        }
        if (!departureTime.equals(flight.departureTime))
        {
            return false;
        }
        if (!from.equals(flight.from))
        {
            return false;
        }
        if (!to.equals(flight.to))
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = 17;
        result = 31 * result + id;
        result = 31 * result + from.hashCode();
        result = 31 * result + to.hashCode();
        result = 31 * result + date.hashCode();
        result = 31 * result + departureTime.hashCode();
        result = 31 * result + arrivalTime.hashCode();
        return result;
    }

    @Override
    public String toString()
    {
        StringBuilder result = new StringBuilder("Flight: ");
        result.append("id=")
                .append(id)
                .append(", from=")
                .append(from)
                .append(", to=")
                .append(to)
                .append(", departureTime=")
                .append(departureTime)
                .append(", arrivalTime=")
                .append(arrivalTime);
        return result.toString();
    }
}
