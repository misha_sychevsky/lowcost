package by.bsuir.data.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Luggage price list
 * @author Michail Sychevsky
 */
public class LuggagePriceList implements Serializable {

    private List<LuggagePrice> luggagePrices;

    public LuggagePriceList()
    {
        luggagePrices = new ArrayList<>();
    }

    public void addLuggagePrice(LuggagePrice luggagePrice)
    {
        luggagePrices.add(luggagePrice);
    }

    public int size()
    {
        return luggagePrices.size();
    }

    public List<LuggagePrice> getLuggagePrices()
    {
        return luggagePrices;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        LuggagePriceList that = (LuggagePriceList) o;

        if (luggagePrices != null ? !luggagePrices.equals(that.luggagePrices) : that.luggagePrices != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        return luggagePrices != null ? luggagePrices.hashCode() : 0;
    }

    @Override
    public String toString()
    {
        StringBuilder result = new StringBuilder("LuggagePriceList:\n");
        for(LuggagePrice price : luggagePrices)
        {
            result.append(price).append("\n");
        }
        return result.toString();
    }
}
