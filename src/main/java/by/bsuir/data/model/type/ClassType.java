package by.bsuir.data.model.type;

/**
 * Class type enum
 * @author Michail Sychevsky
 */
public enum ClassType {

    ECONOMY("Эконом"),
    COMFORT("Комфорт"),
    BUSINESS("Бизнес");

    private final String value;

    private ClassType(String v)
    {
        value = v;
    }

    public String value()
    {
        return value;
    }
}
