package by.bsuir.data.database.exception;

/**
 * This class contains all data error messages
 * @author Michail Sychevsky
 */
public class DataErrorsMessages {

    public static final String ERROR_STATEMENT_NULL = "Statement is null.";
    public static final String ERROR_PREPARED_STATEMENT_NULL = "Prepared statement is null.";
    public static final String ERROR_CONNECTION_NULL = "Connection is null.";
    public static final String ERROR_CLOSING_STATEMENT = "Can't close statement";
    public static final String ERROR_RESULT_SET_NULL = "Result set is null.";
    public static final String ERROR_CLOSING_RESULT_SET = "Can't close result set";
    public static final String ERROR_CLOSING_PREPARED_STATEMENT = "Can't close prepared statement";
    public static final String ERROR_RELEASING_CONNECTION = "Can't release connection";
    public static final String UNSUPPORTED_DB_ERROR = "Error unsupported database.";
    public static final String CONNECTION_DB_ERROR = "Error connecting to database.";
}
