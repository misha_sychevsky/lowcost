package by.bsuir.data.database.exception;

/**
 * Database connection exception
 * @author Michail Sychevsky
 */
public class ConnectionException extends Exception {

    public ConnectionException(String message, Exception e)
    {
        super(message, e);
    }

    public ConnectionException(String message)
    {
        super(message);
    }

    public ConnectionException(Exception e)
    {
        super(DataErrorsMessages.CONNECTION_DB_ERROR, e);
    }
}
