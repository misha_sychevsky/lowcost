package by.bsuir.data.database.exception;

/**
 * Unsupported database exception
 * @author Michail Sychevsky
 */
public class UnsupportedDatabaseException extends Exception {

    public UnsupportedDatabaseException()
    {
        super(DataErrorsMessages.UNSUPPORTED_DB_ERROR);
    }
}
