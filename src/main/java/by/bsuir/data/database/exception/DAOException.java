package by.bsuir.data.database.exception;

/**
 * DAO Exception
 * @author Michail Sychevsky
 */
public class DAOException extends Exception{

    public static final String ERROR_ID = "jsp.text.error.message.dao";

    public DAOException(String message, Exception e)
    {
        super(message, e);
    }

    public DAOException(Exception e)
    {
        super(e);
    }

    public DAOException()
    {
        super();
    }

    public DAOException(String message)
    {
        super(message);
    }
}
