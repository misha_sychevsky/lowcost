package by.bsuir.data.database.dao.mysql;

import by.bsuir.data.database.connection.Connector;
import by.bsuir.data.database.dao.framework.TicketDAO;
import by.bsuir.data.database.dao.util.TypeConverter;
import by.bsuir.data.database.exception.DAOException;
import by.bsuir.data.model.entity.Client;
import by.bsuir.data.model.entity.Flight;
import by.bsuir.data.model.entity.Ticket;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * This interface implements DAO pattern.
 * MySQL Ticket DAO implementation.
 * @author Michail Sychevsky
 */
public class MySQLTicketDAO implements TicketDAO {

    private ResultSet resultSet;

    @Override
    public List<Ticket> getTicketsByClient(Client client) throws DAOException
    {
        Connector connector = new Connector();
        List<Ticket> tickets = new ArrayList<>();
        try
        {
            PreparedStatement statement = connector.getPreparedStatement(MySQLQueries.SQL_GET_TICKETS_BY_CLIENT);
            statement.setInt(1, client.getId());
            resultSet = statement.executeQuery();
            while(resultSet.next())
            {
                tickets.add(makeTicket(client));
            }
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
        return tickets;
    }

    @Override
    public void deleteTicketByClientAndFlight(int clientId, int flightId) throws DAOException
    {
        Connector connector = new Connector();
        try
        {
            PreparedStatement statement = connector.getPreparedStatement(MySQLQueries.SQL_DELETE_TICKET_BY_CLIENT_AND_FLIGHT);
            statement.setInt(1, clientId);
            statement.setInt(2, flightId);
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
    }

    @Override
    public boolean isClientHasTicketOnTheFlight(Client client, Flight flight) throws DAOException
    {
        Connector connector = new Connector();
        boolean result = false;
        try
        {
            PreparedStatement statement = connector.getPreparedStatement(MySQLQueries.SQL_GET_TICKET_BY_CLIENT_AND_FLIGHT);
            statement.setInt(1, client.getId());
            statement.setInt(2, flight.getId());
            resultSet = statement.executeQuery();
            if (resultSet.next())
            {
                result = true;
            }
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
        return result;
    }

    @Override
    public List<Ticket> getTicketsPage(Client client, int start, int count) throws DAOException
    {
        Connector connector = new Connector();
        List<Ticket> tickets = new ArrayList<>();
        try
        {
            PreparedStatement statement = connector.getPreparedStatement(MySQLQueries.SQL_GET_TICKETS_PAGE);
            statement.setInt(1, client.getId());
            statement.setInt(2, start);
            statement.setInt(3, count);
            resultSet = statement.executeQuery();
            while (resultSet.next())
            {
                tickets.add(makeTicket(client));
            }
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
        return tickets;
    }

    @Override
    public void insertTicket(Ticket ticket) throws DAOException
    {
        Connector connector = new Connector();
        try
        {
            PreparedStatement statement = connector.getPreparedStatement(MySQLQueries.SQL_INSERT_TICKET);
            statement.setString(1, ticket.getSeat());
            statement.setInt(2, TypeConverter.processBooleanToDB(ticket.isPrivilegeRB()));
            statement.setInt(3, ticket.getClient().getId());
            statement.setInt(4, ticket.getFlight().getId());
            statement.setString(5, TypeConverter.processClassToDB(ticket.getClassType()));
            statement.setDouble(6, ticket.getPrice());
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
    }

    private Ticket makeTicket(Client client) throws SQLException
    {
        Ticket ticket = new Ticket();
        ticket.setSeat(resultSet.getString(1));
        ticket.setPrivilegeRB(TypeConverter.processBooleanFromDB(resultSet.getInt(2)));
        ticket.setClient(client);
        Flight flight = new Flight();
        flight.setId(resultSet.getInt(4));
        ticket.setClassType(TypeConverter.processClassFromDB(resultSet.getString(5)));
        ticket.setPrice(resultSet.getDouble(6));
        flight.setFrom(resultSet.getString(8));
        flight.setTo(resultSet.getString(9));
        flight.setDate(resultSet.getDate(10));
        flight.setDepartureTime(resultSet.getString(11));
        flight.setArrivalTime(resultSet.getString(12));
        ticket.setFlight(flight);
        return ticket;
    }
}
