package by.bsuir.data.database.dao.mysql;

/**
 * This class contains all sql queries
 * @author Michail Sychevsky
 */
public class MySQLQueries {

    public static final String SQL_GET_ALL_CLIENTS = "select * from clients";
    public static final String SQL_GET_CLIENT_BY_LOGIN = "select * from clients where login=?";
    public static final String SQL_GET_CLIENT_BY_LOGIN_AND_PASSWORD = "select * from clients where login=? and password=?";
    public static final String SQL_INSERT_CLIENT = "insert into clients (first_name, last_name, birth_date, phone, email, login, password, passport_sn, country) "
            + "values (?,?,?,?,?,?,?,?,?)";
    public static final String SQL_UPDATE_CLIENT = "update clients SET first_name=?, last_name=?, birth_date=?, phone=?, email=?, login=?, password=?, passport_sn=?, country=?"
            + "WHERE id_client=?";

    public static final String SQL_GET_ALL_FLIGHTS = "select * from flights";

    public static final String SQL_GET_ALL_LUGGAGE = "select * from luggage";
    public static final String SQL_INSERT_LUGGAGE = "insert into luggage (weight, volume, client_id, flight_number_id, price) "
            + "values (?,?,?,?,?)";
    public static final String SQL_DELETE_LUGGAGE_BY_ID = "delete from luggage where id_luggage_number=?";
    public static final String SQL_GET_LUGGAGE_PAGE = "select * from luggage inner join flights on luggage.flight_number_id = flights.id_flight_number where client_id=? limit ?,?";

    public static final String SQL_GET_ALL_TICKETS = "select * from tickets";
    public static final String SQL_INSERT_TICKET = "insert into tickets (seat, privilege_rb, client, flight_number, class, price) "
            + "values (?,?,?,?,?,?)";
    public static final String SQL_GET_TICKETS_BY_CLIENT = "select * from tickets inner join flights on tickets.flight_number = flights.id_flight_number where client=?";
    public static final String SQL_DELETE_TICKET_BY_CLIENT_AND_FLIGHT = "delete from tickets where client=? and flight_number=?";
    public static final String SQL_GET_TICKET_BY_CLIENT_AND_FLIGHT = "select * from tickets where client=? and flight_number=?";
    public static final String SQL_GET_TICKETS_PAGE = "select * from tickets inner join flights on tickets.flight_number = flights.id_flight_number where client=? limit ?,?";

    public static final String SQL_GET_ALL_TICKET_PRICES = "select id_flight_number, date_flight, class, free_seats_count, price "
            + "from flights inner join "
            + "(select id_flight_number as flight_number, free_seats_count, class, price from ticket_prices) as prices "
            + "on flights.id_flight_number = prices.flight_number";
    public static final String SQL_GET_TICKET_PRICES_PAGE_BY_SEARCH_PARAMETERS = "select id_flight_number, departure_time, arrival_time, privilege_rb_price, price "
            + "from flights inner join "
            + "(select id_flight_number as flight_number, free_seats_count, class, privilege_rb_price, price from ticket_prices) as prices "
            + "on flights.id_flight_number = prices.flight_number "
            + "where from_city=? and to_city=? and date_flight=? and class=? and free_seats_count !=\"0\" limit ?,?";
    public static final String SQL_GET_TICKET_PRICE_BY_SELECTED = "select from_city, to_city, date_flight, departure_time, arrival_time, free_seats_count, privilege_rb_price, price "
            + "from flights inner join "
            + "(select id_flight_number as flight_number, free_seats_count, class, privilege_rb_price, price from ticket_prices) as prices "
            + "on flights.id_flight_number = prices.flight_number "
            + "where id_flight_number=? and class=?";
    public static final String SQL_GET_FREE_SEATS_COUNT_BY_FLIGHT_AND_CLASS = "select free_seats_count from ticket_prices where id_flight_number=? and class=?";
    public static final String SQL_DECREMENT_FREE_SEATS_COUNT_BY_FLIGHT_AND_CLASS = "update ticket_prices set free_seats_count = free_seats_count - 1 where id_flight_number=? and class=?";
    public static final String SQL_INCREMENT_FREE_SEATS_COUNT_BY_FLIGHT_AND_CLASS = "update ticket_prices set free_seats_count = free_seats_count + 1 where id_flight_number=? and class=?";
    public static final String SQL_UPDATE_TICKET_PRICE = "update ticket_prices set price=? where id_flight_number=? and class=?";

}
