package by.bsuir.data.database.dao.mysql;

import by.bsuir.data.database.connection.Connector;
import by.bsuir.data.database.dao.framework.LuggageDAO;
import by.bsuir.data.model.entity.Client;
import by.bsuir.data.model.entity.Flight;
import by.bsuir.data.model.entity.Luggage;
import by.bsuir.data.database.exception.DAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * This interface implements DAO pattern.
 * MySQL Luggage DAO implementation.
 * @author Michail Sychevsky
 */
public class MySQLLuggageDAO implements LuggageDAO {

    private ResultSet resultSet;

    @Override
    public void insertLuggage(Luggage luggage) throws DAOException
    {
        Connector connector = new Connector();
        try
        {
            PreparedStatement statement = connector.getPreparedStatement(MySQLQueries.SQL_INSERT_LUGGAGE);
            statement.setDouble(1, luggage.getWeight());
            statement.setDouble(2, luggage.getVolume());
            statement.setInt(3, luggage.getClient().getId());
            statement.setInt(4, luggage.getFlight().getId());
            statement.setDouble(5, luggage.getPrice());
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
    }

    @Override
    public void deleteLuggageById(int luggageId) throws DAOException
    {
        Connector connector = new Connector();
        try
        {
            PreparedStatement statement = connector.getPreparedStatement(MySQLQueries.SQL_DELETE_LUGGAGE_BY_ID);
            statement.setInt(1, luggageId);
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
    }

    @Override
    public List<Luggage> getLuggagePage(Client client, int start, int count) throws DAOException
    {
        Connector connector = new Connector();
        List<Luggage> luggage = new ArrayList<>();
        try
        {
            PreparedStatement statement = connector.getPreparedStatement(MySQLQueries.SQL_GET_LUGGAGE_PAGE);
            statement.setInt(1, client.getId());
            statement.setInt(2, start);
            statement.setInt(3, count);
            resultSet = statement.executeQuery();
            while(resultSet.next())
            {
                luggage.add(makeLuggage(client));
            }
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
        return luggage;
    }

    private Luggage makeLuggage(Client client) throws SQLException
    {
        Luggage luggage = new Luggage();
        luggage.setId(resultSet.getInt(1));
        luggage.setWeight(resultSet.getDouble(2));
        luggage.setVolume(resultSet.getDouble(3));
        luggage.setClient(client);
        luggage.setPrice(resultSet.getDouble(6));
        Flight flight = new Flight();
        flight.setId(resultSet.getInt(5));
        flight.setFrom(resultSet.getString(8));
        flight.setTo(resultSet.getString(9));
        flight.setDate(resultSet.getDate(10));
        flight.setDepartureTime(resultSet.getString(11));
        flight.setArrivalTime(resultSet.getString(12));
        luggage.setFlight(flight);
        return luggage;
    }
}
