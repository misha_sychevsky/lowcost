package by.bsuir.data.database.dao.framework;

import by.bsuir.data.model.entity.Client;
import by.bsuir.data.model.entity.Flight;
import by.bsuir.data.model.entity.Ticket;
import by.bsuir.data.database.exception.DAOException;

import java.util.List;

/**
 * This interface implements DAO pattern.
 * Ticket DAO.
 * @author Michail Sychevsky
 */
public interface TicketDAO {

    void insertTicket(Ticket ticket) throws DAOException;
    void deleteTicketByClientAndFlight(int clientId, int flightId) throws DAOException;
    boolean isClientHasTicketOnTheFlight(Client client, Flight flight) throws DAOException;
    List<Ticket> getTicketsPage(Client client, int start, int count) throws DAOException;
    List<Ticket> getTicketsByClient(Client client) throws DAOException;
}
