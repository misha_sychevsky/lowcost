package by.bsuir.data.database.dao.mysql;

import by.bsuir.data.database.connection.Connector;
import by.bsuir.data.database.dao.framework.ClientDAO;
import by.bsuir.data.model.entity.Client;
import by.bsuir.data.database.exception.DAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * This interface implements DAO pattern.
 * MySQL Client DAO implementation.
 * @author Michail Sychevsky
 */
public class MySQLClientDAO implements ClientDAO {

    private ResultSet resultSet;

    @Override
    public void insertClient(Client client) throws DAOException
    {
        Connector connector = new Connector();
        try
        {
            PreparedStatement statement = connector.getPreparedStatement(MySQLQueries.SQL_INSERT_CLIENT);
            statement.setString(1, client.getFirstName());
            statement.setString(2, client.getLastName());
            statement.setDate(3, client.getBirthDate());
            statement.setString(4, client.getPhone());
            statement.setString(5, client.getEmail());
            statement.setString(6, client.getLogin());
            statement.setString(7, client.getPassword());
            statement.setString(8, client.getPassportNS());
            statement.setString(9, client.getCountry());
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
    }

    @Override
    public void updateClient(Client client) throws DAOException
    {
        Connector connector = new Connector();
        try
        {
            PreparedStatement statement = connector.getPreparedStatement(MySQLQueries.SQL_UPDATE_CLIENT);
            statement.setString(1, client.getFirstName());
            statement.setString(2, client.getLastName());
            statement.setDate(3, client.getBirthDate());
            statement.setString(4, client.getPhone());
            statement.setString(5, client.getEmail());
            statement.setString(6, client.getLogin());
            statement.setString(7, client.getPassword());
            statement.setString(8, client.getPassportNS());
            statement.setString(9, client.getCountry());
            statement.setInt(10, client.getId());
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
    }

    @Override
    public boolean isClientExists(Client client) throws DAOException
    {
        Connector connector = new Connector();
        boolean result = false;
        try
        {
            PreparedStatement statement = connector.getPreparedStatement(MySQLQueries.SQL_GET_CLIENT_BY_LOGIN);
            statement.setString(1, client.getLogin());
            resultSet = statement.executeQuery();
            if (resultSet.next())
            {
                result = true;
            }
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
        return result;
    }

    @Override
    public List<Client> getAllClients() throws DAOException
    {
        Connector connector = new Connector();
        List<Client> clients = new ArrayList<>();
        try
        {
            Statement statement = connector.getStatement();
            resultSet = statement.executeQuery(MySQLQueries.SQL_GET_ALL_CLIENTS);
            while(resultSet.next())
            {
                clients.add(makeClient());
            }
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
        return clients;
    }

    @Override
    public Client getClientByAuthDate(String login, String password) throws DAOException
    {
        Connector connector = new Connector();
        Client client = null;
        try
        {
            PreparedStatement statement = connector.getPreparedStatement(MySQLQueries.SQL_GET_CLIENT_BY_LOGIN_AND_PASSWORD);
            statement.setString(1, login);
            statement.setString(2, password);
            resultSet = statement.executeQuery();
            while (resultSet.next())
            {
                client = makeClient();
            }
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
        return client;
    }

    private Client makeClient() throws SQLException
    {
        Client client = new Client();
        client.setId(resultSet.getInt(1));
        client.setFirstName(resultSet.getString(2));
        client.setLastName(resultSet.getString(3));
        client.setBirthDate(resultSet.getDate(4));
        client.setPhone(resultSet.getString(5));
        client.setEmail(resultSet.getString(6));
        client.setLogin(resultSet.getString(7));
        client.setPassword(resultSet.getString(8));
        client.setPassportNS(resultSet.getString(9));
        client.setCountry(resultSet.getString(10));
        return client;
    }
}
