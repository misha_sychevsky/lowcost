package by.bsuir.data.database.dao.mysql;

import by.bsuir.data.database.dao.AbstractDAOFactory;
import by.bsuir.data.database.dao.framework.*;


/**
 * This interface implements DAO pattern.
 * MySQL DAO factory return the one of the MySQL DAO implementation objects
 * @author Michail Sychevsky
 */
public class MySQLDAOFactory extends AbstractDAOFactory {

    @Override
    public ClientDAO getClientDAO()
    {
        return new MySQLClientDAO();
    }

    @Override
    public FlightDAO getFlightDAO()
    {
        return new MySQLFlightDAO();
    }

    @Override
    public LuggageDAO getLuggageDAO()
    {
        return new MySQLLuggageDAO();
    }

    @Override
    public TicketDAO getTicketDAO()
    {
        return new MySQLTicketDAO();
    }

    @Override
    public TicketPriceDAO getTicketPriceDAO()
    {
        return new MySQLTicketPriceDAO();
    }
}
