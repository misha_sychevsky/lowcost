package by.bsuir.data.database.dao.framework;

import by.bsuir.data.model.entity.Client;
import by.bsuir.data.model.entity.Luggage;
import by.bsuir.data.database.exception.DAOException;

import java.util.List;

/**
 * This interface implements DAO pattern.
 * Luggage DAO.
 * @author Michail Sychevsky
 */
public interface LuggageDAO {

    void insertLuggage(Luggage luggage) throws DAOException;
    void deleteLuggageById(int luggageId) throws DAOException;
    List<Luggage> getLuggagePage(Client client, int start, int count) throws DAOException;
}
