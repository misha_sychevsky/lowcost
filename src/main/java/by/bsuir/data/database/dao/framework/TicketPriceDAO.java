package by.bsuir.data.database.dao.framework;


import by.bsuir.data.model.entity.TicketPrice;
import by.bsuir.data.database.exception.DAOException;
import by.bsuir.data.model.type.ClassType;

import java.sql.Date;
import java.util.List;

/**
 * This interface implements DAO pattern.
 * Ticket price DAO.
 * @author Michail Sychevsky
 */
public interface TicketPriceDAO {

    void decrementFreeSeatsCount(int flightNumber, ClassType classType) throws DAOException;
    void incrementFreeSeatsCount(int flightNumber, ClassType classType) throws DAOException;
    List<TicketPrice> getTicketPricesPageBySearchParameters(String from, String to, Date date,
                                                            ClassType classType, int start, int count) throws DAOException;
    TicketPrice getTicketPriceBySelected(int flightNumber, ClassType classType) throws DAOException;
    void updateTicketPrice(int flightNumber, ClassType classType, double price) throws DAOException;
    List<TicketPrice> getAllTicketPrices() throws DAOException;
}
