package by.bsuir.data.database.dao.util;

import by.bsuir.data.model.type.ClassType;

/**
 * Static class to convert types to/from database.
 * @author Michail Sychevsky
 */
public class TypeConverter {

    public static int processBooleanToDB(boolean expression)
    {
        if(expression)
        {
            return 1;
        }
        return 0;
    }

    public static boolean processBooleanFromDB(int expression)
    {
        return expression == 1;
    }

    public static ClassType processClassFromDB(String classType)
    {
        if(classType.equals(ClassType.ECONOMY.value()))
        {
            return ClassType.ECONOMY;
        }
        else
        {
            return ClassType.BUSINESS;
        }
    }

    public static String processClassToDB(ClassType classType)
    {
        return classType.value();
    }
}
