package by.bsuir.data.database.dao;

import by.bsuir.data.database.dao.framework.*;
import by.bsuir.data.database.dao.mysql.MySQLDAOFactory;
import by.bsuir.data.database.exception.UnsupportedDatabaseException;

/**
 * This interface implements DAO pattern.
 * Abstract DAO factory returns the object of DAO factory for used database.
 * @author Michail Sychevsky
 */
public abstract class AbstractDAOFactory {

    private static final String DATABASE_MYSQL = "mysql";

    public abstract ClientDAO getClientDAO();
    public abstract FlightDAO getFlightDAO();
    public abstract LuggageDAO getLuggageDAO();
    public abstract TicketDAO getTicketDAO();
    public abstract TicketPriceDAO getTicketPriceDAO();

    public static AbstractDAOFactory getDAOFactory(String name) throws UnsupportedDatabaseException
    {
        switch (name)
        {
            case DATABASE_MYSQL:
                return new MySQLDAOFactory();

            default:
                throw new UnsupportedDatabaseException();
        }
    }
}
