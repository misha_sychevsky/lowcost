package by.bsuir.data.database.dao.framework;

import by.bsuir.data.model.entity.Flight;
import by.bsuir.data.database.exception.DAOException;
import by.bsuir.data.model.type.ClassType;

import java.util.Date;
import java.util.List;

/**
 * This interface implements DAO pattern.
 * Flight DAO.
 * @author Michail Sychevsky
 */
public interface FlightDAO {

    int insertFlight(Flight flight) throws DAOException;
    int deleteFlight(Flight flight) throws DAOException;
    int updateFlight(Flight flight) throws DAOException;
    List<Flight> getAllFlights() throws DAOException;
}
