package by.bsuir.data.database.dao.framework;

import by.bsuir.data.model.entity.Client;
import by.bsuir.data.database.exception.DAOException;

import java.util.List;

/**
 * This interface implements DAO pattern.
 * Client DAO.
 * @author Michail Sychevsky
 */
public interface ClientDAO {

    void insertClient(Client client) throws DAOException;
    void updateClient(Client client) throws DAOException;
    boolean isClientExists(Client client) throws DAOException;
    List<Client> getAllClients() throws DAOException;
    Client getClientByAuthDate(String login, String password) throws DAOException;
}
