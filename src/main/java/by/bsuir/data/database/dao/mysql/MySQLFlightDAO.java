package by.bsuir.data.database.dao.mysql;

import by.bsuir.data.database.connection.Connector;
import by.bsuir.data.database.dao.framework.FlightDAO;
import by.bsuir.data.model.entity.Flight;
import by.bsuir.data.database.exception.DAOException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * This interface implements DAO pattern.
 * MySQL Flight DAO implementation.
 * @author Michail Sychevsky
 */
public class MySQLFlightDAO implements FlightDAO {

    private ResultSet resultSet;

    @Override
    public List<Flight> getAllFlights() throws DAOException
    {
        Connector connector = new Connector();
        List<Flight> flights = new ArrayList<>();
        try
        {
            Statement statement = connector.getStatement();
            resultSet = statement.executeQuery(MySQLQueries.SQL_GET_ALL_FLIGHTS);
            while(resultSet.next())
            {
                flights.add(makeFlight());
            }
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
           connector.close(resultSet);
        }
        return flights;
    }

    @Override
    public int updateFlight(Flight flight) throws DAOException
    {
        return 0;
    }

    @Override
    public int deleteFlight(Flight flight) throws DAOException
    {
        return 0;
    }

    @Override
    public int insertFlight(Flight flight) throws DAOException
    {
        return 0;
    }

    private Flight makeFlight() throws SQLException
    {
        Flight flight = new Flight();
        flight.setId(resultSet.getInt(1));
        flight.setFrom(resultSet.getString(2));
        flight.setTo(resultSet.getString(3));
        flight.setDate(resultSet.getDate(4));
        flight.setDepartureTime(resultSet.getString(5));
        flight.setArrivalTime(resultSet.getString(6));
        return flight;
    }
}
