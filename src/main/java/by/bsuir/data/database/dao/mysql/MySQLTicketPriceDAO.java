package by.bsuir.data.database.dao.mysql;

import by.bsuir.data.database.connection.Connector;
import by.bsuir.data.database.dao.framework.TicketPriceDAO;
import by.bsuir.data.database.dao.util.TypeConverter;
import by.bsuir.data.model.entity.Flight;
import by.bsuir.data.model.entity.TicketPrice;
import by.bsuir.data.database.exception.DAOException;
import by.bsuir.data.model.type.ClassType;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This interface implements DAO pattern.
 * MySQL Ticket price DAO implementation.
 * @author Michail Sychevsky
 */
public class MySQLTicketPriceDAO implements TicketPriceDAO {

    private ResultSet resultSet;

    @Override
    public void decrementFreeSeatsCount(int flightNumber, ClassType classType) throws DAOException
    {
        Connector connector = new Connector();
        try
        {
            PreparedStatement statement = connector.getPreparedStatement(MySQLQueries.SQL_GET_FREE_SEATS_COUNT_BY_FLIGHT_AND_CLASS);
            statement.setInt(1, flightNumber);
            statement.setString(2, TypeConverter.processClassToDB(classType));
            resultSet = statement.executeQuery();

            int freeSeatsCount = 0;
            while (resultSet.next())
            {
                freeSeatsCount = resultSet.getInt(1);
            }
            if(freeSeatsCount != 0)
            {
                statement = connector.getPreparedStatement(MySQLQueries.SQL_DECREMENT_FREE_SEATS_COUNT_BY_FLIGHT_AND_CLASS);
                statement.setInt(1, flightNumber);
                statement.setString(2, TypeConverter.processClassToDB(classType));
                statement.executeUpdate();
            }
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
    }

    @Override
    public void incrementFreeSeatsCount(int flightNumber, ClassType classType) throws DAOException
    {
        Connector connector = new Connector();
        try
        {
            PreparedStatement statement = connector.getPreparedStatement(MySQLQueries.SQL_INCREMENT_FREE_SEATS_COUNT_BY_FLIGHT_AND_CLASS);
            statement.setInt(1, flightNumber);
            statement.setString(2, TypeConverter.processClassToDB(classType));
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
    }

    @Override
    public List<TicketPrice> getTicketPricesPageBySearchParameters(String from, String to, Date date,
                                                               ClassType classType, int start, int count) throws DAOException
    {
        Connector connector = new Connector();
        ArrayList<TicketPrice> ticketPrices = new ArrayList<>();
        try
        {
            PreparedStatement statement = connector.getPreparedStatement(MySQLQueries.SQL_GET_TICKET_PRICES_PAGE_BY_SEARCH_PARAMETERS);
            statement.setString(1, from);
            statement.setString(2, to);
            statement.setDate(3, date);
            statement.setString(4, TypeConverter.processClassToDB(classType));
            statement.setInt(5, start);
            statement.setInt(6, count);
            resultSet = statement.executeQuery();
            while (resultSet.next())
            {
                Flight flight = new Flight();
                flight.setId(resultSet.getInt(1));
                flight.setDepartureTime(resultSet.getString(2));
                flight.setArrivalTime(resultSet.getString(3));
                TicketPrice ticketPrice = new TicketPrice();
                ticketPrice.setFlight(flight);
                ticketPrice.setPrivilegeRBPrice(resultSet.getDouble(4));
                ticketPrice.setPrice(resultSet.getDouble(5));
                ticketPrices.add(ticketPrice);
            }
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
        return ticketPrices;
    }

    @Override
    public TicketPrice getTicketPriceBySelected(int flightNumber, ClassType classType) throws DAOException
    {
        Connector connector = new Connector();
        TicketPrice ticketPrice = new TicketPrice();
        try
        {
            PreparedStatement statement = connector.getPreparedStatement(MySQLQueries.SQL_GET_TICKET_PRICE_BY_SELECTED);
            statement.setInt(1, flightNumber);
            statement.setString(2, TypeConverter.processClassToDB(classType));
            resultSet = statement.executeQuery();
            while (resultSet.next())
            {
                Flight flight = new Flight();
                flight.setId(flightNumber);
                flight.setFrom(resultSet.getString(1));
                flight.setTo(resultSet.getString(2));
                flight.setDate(resultSet.getDate(3));
                flight.setDepartureTime(resultSet.getString(4));
                flight.setArrivalTime(resultSet.getString(5));
                ticketPrice.setFlight(flight);
                ticketPrice.setClassType(classType);
                ticketPrice.setFreeSeatsCount(resultSet.getInt(6));
                ticketPrice.setPrivilegeRBPrice(resultSet.getDouble(7));
                ticketPrice.setPrice(resultSet.getDouble(8));
            }
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
        return ticketPrice;
    }

    @Override
    public void updateTicketPrice(int flightNumber, ClassType classType, double price) throws DAOException
    {
        Connector connector = new Connector();
        try
        {
            PreparedStatement statement = connector.getPreparedStatement(MySQLQueries.SQL_UPDATE_TICKET_PRICE);
            statement.setDouble(1, price);
            statement.setInt(2, flightNumber);
            statement.setString(3, TypeConverter.processClassToDB(classType));
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
    }

    @Override
    public List<TicketPrice> getAllTicketPrices() throws DAOException
    {
        Connector connector = new Connector();
        List<TicketPrice> ticketPrices = new ArrayList<>();
        try
        {
            Statement statement = connector.getStatement();
            resultSet = statement.executeQuery(MySQLQueries.SQL_GET_ALL_TICKET_PRICES);
            while(resultSet.next())
            {
                ticketPrices.add(makeTicketPrice());
            }
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally
        {
            connector.close(resultSet);
        }
        return ticketPrices;
    }

    private TicketPrice makeTicketPrice() throws SQLException
    {
        TicketPrice ticketPrice = new TicketPrice();
        Flight flight = new Flight();
        flight.setId(resultSet.getInt(1));
        flight.setDate(resultSet.getDate(2));
        ticketPrice.setFlight(flight);
        ticketPrice.setClassType(TypeConverter.processClassFromDB(resultSet.getString(3)));
        ticketPrice.setFreeSeatsCount(resultSet.getInt(4));
        ticketPrice.setPrice(resultSet.getDouble(5));
        return ticketPrice;
    }
}
