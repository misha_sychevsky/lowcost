package by.bsuir.data.database.connection;

import by.bsuir.data.database.exception.ConnectionException;
import by.bsuir.data.database.exception.DAOException;
import by.bsuir.data.database.exception.DataErrorsMessages;
import org.apache.log4j.Logger;

import java.sql.*;

/**
 * Database connector class
 * This class is necessary to get statements to execute and to close it.
 * @author Michail Sychevsky
 */
public class Connector {

    private static final Logger LOGGER = Logger.getLogger(Connector.class);

    private Connection connection;
    private Statement statement;
    private PreparedStatement preparedStatement;
    private ConnectionPool connectionPool;

    public Connector() throws DAOException
    {
        try
        {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
        }
        catch (ConnectionException e)
        {
            throw new DAOException(e);
        }
    }

    public Statement getStatement() throws DAOException
    {
        if (connection != null)
        {
            try
            {
                statement = connection.createStatement();
                if (statement != null)
                {
                    return statement;
                }
            }
            catch (SQLException e)
            {
                throw new DAOException(e);
            }
        }
        throw new DAOException(DataErrorsMessages.ERROR_CONNECTION_NULL);
    }

    public PreparedStatement getPreparedStatement(String sql) throws DAOException
    {
        if (connection != null)
        {
            try
            {
                preparedStatement = connection.prepareStatement(sql);
                if (preparedStatement != null)
                {
                    return preparedStatement;
                }
            }
            catch (SQLException e)
            {
                throw new DAOException(e);
            }
        }
        throw new DAOException(DataErrorsMessages.ERROR_CONNECTION_NULL);
    }

    public void close(ResultSet resultSet)
    {
        closeStatement();
        closePreparedStatement();
        closeResultSet(resultSet);
        closeConnection();
    }

    private void closeStatement()
    {
        if (statement != null)
        {
            try
            {
                statement.close();
            }
            catch (SQLException e)
            {
                LOGGER.error(DataErrorsMessages.ERROR_CLOSING_STATEMENT, e);
            }
        }
        else
        {
            LOGGER.warn(DataErrorsMessages.ERROR_STATEMENT_NULL);
        }
    }

    private void closePreparedStatement()
    {
        if (preparedStatement != null)
        {
            try
            {
                preparedStatement.close();
            }
            catch (SQLException e)
            {
                LOGGER.error(DataErrorsMessages.ERROR_CLOSING_PREPARED_STATEMENT, e);
            }
        }
        else
        {
            LOGGER.warn(DataErrorsMessages.ERROR_PREPARED_STATEMENT_NULL);
        }
    }

    private void closeResultSet(ResultSet resultSet)
    {
        if(resultSet != null)
        {
            try
            {
                resultSet.close();
            }
            catch(SQLException e)
            {
                LOGGER.warn(DataErrorsMessages.ERROR_CLOSING_RESULT_SET);
            }
        }
        else
        {
            LOGGER.warn(DataErrorsMessages.ERROR_RESULT_SET_NULL);
        }
    }

    private void closeConnection()
    {
        if (connection != null)
        {
            try
            {
                connectionPool.putConnection(connection);
            }
            catch (ConnectionException e)
            {
                LOGGER.error(DataErrorsMessages.ERROR_RELEASING_CONNECTION, e);
            }
        }
        else
        {
            LOGGER.warn(DataErrorsMessages.ERROR_CONNECTION_NULL);
        }
    }
}
