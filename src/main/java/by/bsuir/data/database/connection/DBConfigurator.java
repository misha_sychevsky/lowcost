package by.bsuir.data.database.connection;

import by.bsuir.util.global.Configurator;

import java.util.ResourceBundle;

/**
 * This class get the database property from properties by used database
 * @author Michail Sychevsky
 */
public class DBConfigurator {

    private static final String RESOURCES = "database";
    private static final String USE_DB = "use_database";

    private static String dbPrefix;
    private static ResourceBundle dbProperties;

    private volatile static DBConfigurator instance;

    private DBConfigurator()
    {
        dbProperties = ResourceBundle.getBundle(RESOURCES);
        dbPrefix = Configurator.getGlobalProperty(USE_DB);
    }

    public static DBConfigurator getInstance()
    {
        if(instance == null)
        {
            synchronized (ConnectionPool.class)
            {
                if (instance == null)
                {
                    instance = new DBConfigurator();
                }
            }
        }
        return instance;
    }

    public String getDBProperty(String name)
    {
        return dbProperties.getString(dbPrefix + "." + name);
    }
}
