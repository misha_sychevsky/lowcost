package by.bsuir.data.database.connection;

import by.bsuir.data.database.exception.ConnectionException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Database connection pool.
 * Initialize 10 connections with used database when application starts.
 * @author Michail Sychevsky
 */
public class ConnectionPool {

    private static final String URL_STR = "url";
    private static final String USER_STR = "user";
    private static final String PASS_STR = "password";
    private static final String DRIVER_STR = "driver";
    private static final String CONNECTIONS_COUNT_STR = "connections_count";

    private volatile static ConnectionPool instance;

    private BlockingQueue<Connection> availableConnections;

    private ConnectionPool() throws ConnectionException
    {
        DBConfigurator handler = DBConfigurator.getInstance();
        String driver = handler.getDBProperty(DRIVER_STR);

        try
        {
            Class.forName(driver);
        }
        catch (ClassNotFoundException e)
        {
            throw new ConnectionException(e);
        }

        String url = handler.getDBProperty(URL_STR);
        String user = handler.getDBProperty(USER_STR);
        String pass = handler.getDBProperty(PASS_STR);
        int connectionsCount = Integer.parseInt(handler.getDBProperty(CONNECTIONS_COUNT_STR));

        availableConnections = new ArrayBlockingQueue<>(connectionsCount, true);
        for(int count = 0; count < connectionsCount; ++count)
        {
            try
            {
                availableConnections.put(initializeConnection(url, user, pass));
            }
            catch (InterruptedException e)
            {
                throw new ConnectionException(e);
            }
        }
    }

    /**
     * Initialize connection with used database with driver
     *
     * @param url
     * @param user
     * @param pass
     * @return
     * @throws ConnectionException
     */
    private Connection initializeConnection(String url, String user, String pass) throws ConnectionException
    {
        Connection conn;
        try
        {
            conn = DriverManager.getConnection(url, user, pass);
        }
        catch (SQLException e)
        {
            throw new ConnectionException(e);
        }
        return conn;
    }

    public static ConnectionPool getInstance() throws ConnectionException
    {
        if (instance == null)
        {
            synchronized (ConnectionPool.class)
            {
                if (instance == null)
                {
                    instance = new ConnectionPool();
                }
            }
        }
        return instance;
    }

    /**
     * Take connection form connection pool.
     *
     * @return
     * @throws ConnectionException
     */
    public Connection takeConnection() throws ConnectionException
    {
        Connection connection;
        try
        {
            connection = availableConnections.take();
        }
        catch (InterruptedException e)
        {
            throw new ConnectionException(e);
        }
        return connection;
    }

    /**
     * Put connection to connection pool.
     *
     * @return
     * @throws ConnectionException
     */
    public void putConnection(Connection connection) throws ConnectionException
    {
        try
        {
            availableConnections.put(connection);
        }
        catch (InterruptedException e)
        {
            throw new ConnectionException(e);
        }
    }
}
