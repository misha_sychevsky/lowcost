package by.bsuir.data.xml.dao;

import by.bsuir.data.model.builder.LuggagePriceBuilder;
import by.bsuir.data.model.entity.LuggagePrice;
import by.bsuir.data.model.entity.LuggagePriceList;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * This class parse the xml document.
 * @author Michail Sychevsky
 */
public class XMLHandler extends DefaultHandler {

    private static final Logger LOGGER = Logger.getLogger(XMLHandler.class);

    private LuggagePriceList priceList;
    private LuggagePriceBuilder builder;

    public XMLHandler(LuggagePriceList priceList)
    {
        this.priceList = priceList;
    }

    @Override
    public void startDocument() throws SAXException
    {
        LOGGER.info("Start parsing xml document");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
    {
        if(qName.equals(XMLConstants.TAG_LUGGAGE_PRICE))
        {
            builder = new LuggagePriceBuilder();
        }

        if(qName.equals(XMLConstants.TAG_VOLUME))
        {
            builder.setMinVolume(Double.parseDouble(attributes.getValue(XMLConstants.ATTR_MIN)));
            builder.setMaxVolume(Double.parseDouble(attributes.getValue(XMLConstants.ATTR_MAX)));
        }

        if(qName.equals(XMLConstants.TAG_WEIGHT))
        {
            builder.setMinWeight(Double.parseDouble(attributes.getValue(XMLConstants.ATTR_MIN)));
            builder.setMaxWeight(Double.parseDouble(attributes.getValue(XMLConstants.ATTR_MAX)));
        }

        if(qName.equals((XMLConstants.TAG_PRICE)))
        {
            builder.setPrice(Double.parseDouble(attributes.getValue(XMLConstants.ATTR_VALUE)));
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException
    {
        if(qName.equals(XMLConstants.TAG_LUGGAGE_PRICE))
        {
            LuggagePrice luggagePrice = builder.build();
            priceList.addLuggagePrice(luggagePrice);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException
    {

    }

    @Override
    public void endDocument() throws SAXException
    {
        LOGGER.info("End of parsing xml");
        LOGGER.info("LuggagePrise entities count: " + priceList.size());
    }
}
