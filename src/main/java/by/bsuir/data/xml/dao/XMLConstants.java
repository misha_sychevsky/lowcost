package by.bsuir.data.xml.dao;

/**
 * This class contains all constants for xml document parsing.
 * @author Michail Sychevsky
 */
class XMLConstants {

    public static final String TAG_LUGGAGE_PRICE = "luggagePrice";
    public static final String TAG_VOLUME = "volume";
    public static final String TAG_WEIGHT = "weight";
    public static final String TAG_PRICE = "price";

    public static final String ATTR_MIN = "min";
    public static final String ATTR_MAX = "max";
    public static final String ATTR_VALUE = "value";
}
