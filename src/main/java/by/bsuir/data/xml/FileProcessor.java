package by.bsuir.data.xml;

import by.bsuir.data.model.entity.LuggagePriceList;
import by.bsuir.data.xml.dao.XMLHandler;
import by.bsuir.data.xml.exception.XMLException;
import by.bsuir.util.global.Configurator;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;

/**
 * This class is necessary to get information from xml document.
 * @author Michail Sychevsky
 */
public class FileProcessor {

    private static final String PATH = "file_path";
    private LuggagePriceList priceList;

    private volatile static FileProcessor instance;

    private FileProcessor() throws XMLException
    {
        priceList = new LuggagePriceList();
        processFile();
    }

    /**
     * Open end process xml file.
     *
     * Add information from file to price list.
     * @throws XMLException
     */
    private void processFile() throws XMLException
    {
        InputStream xmlStream = FileProcessor.class.getResourceAsStream(Configurator.getGlobalProperty(PATH));
        XMLHandler handler = new XMLHandler(priceList);
        try
        {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            parser.parse(xmlStream, handler);

        }
        catch (ParserConfigurationException | SAXException | IOException e)
        {
            throw new XMLException(e);
        }
    }

    public static FileProcessor getInstance() throws XMLException
    {
        if (instance == null)
        {
            synchronized (FileProcessor.class)
            {
                if (instance == null)
                {
                    instance = new FileProcessor();
                }
            }
        }
        return instance;
    }

    public LuggagePriceList getPriceList()
    {
        return priceList;
    }
}
