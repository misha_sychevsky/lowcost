package by.bsuir.data.xml.exception;

/**
 * XML Exception.
 * @author Michail Sychevsky
 */
public class XMLException extends Exception {

    public static final String ERROR_ID = "jsp.text.error.xml";

    public XMLException(String message, Exception e)
    {
        super(message, e);
    }

    public XMLException(String message)
    {
        super(message);
    }

    public XMLException(Exception e)
    {
        super(e);
    }
}
