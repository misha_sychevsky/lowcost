package by.bsuir.controller.filter;

import by.bsuir.business.service.CommandConstants;
import by.bsuir.util.global.Configurator;

import javax.servlet.*;
import java.io.IOException;

/**
 * Deleting ticket filter.
 * This class is necessary for checking the flight id from request.
 * @author Michail Sychevsky
 */
public class DeleteTicketsFilter implements Filter {

    @Override
    public void init(FilterConfig config) throws ServletException
    {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        for(int itemCount = 1; itemCount <= CommandConstants.TABLE_TICKETS_ITEMS_COUNT_ON_PAGE; ++itemCount)
        {
            String flightId = request.getParameter(CommandConstants.PARAMETER_FLIGHT_ID + "-" + itemCount);
            if(flightId != null)
            {
                try
                {
                    Integer.parseInt(flightId);
                }
                catch (NumberFormatException e)
                {
                    request.setAttribute(CommandConstants.ATTRIBUTE_FLAG, CommandConstants.FLAG_TICKET_DELETE_ERROR);
                    request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_CLIENT_PAGE)).forward(request, response);
                }
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy()
    {

    }
}
