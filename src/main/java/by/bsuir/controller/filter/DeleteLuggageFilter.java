package by.bsuir.controller.filter;

import by.bsuir.business.service.CommandConstants;
import by.bsuir.util.global.Configurator;

import javax.servlet.*;
import java.io.IOException;

/**
 * Deleting luggage filter.
 * This class is necessary for checking the flight id from request.
 * @author Michail Sychevsky
 */
public class DeleteLuggageFilter implements Filter {

    @Override
    public void init(FilterConfig config) throws ServletException
    {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        for(int itemCount = 1; itemCount <= CommandConstants.TABLE_LUGGAGE_ITEMS_COUNT_ON_PAGE; ++itemCount)
        {
            String luggageId = request.getParameter(CommandConstants.PARAMETER_LUGGAGE_ID + "-" + itemCount);
            if(luggageId != null)
            {
                try
                {
                    Integer.parseInt(luggageId);
                }
                catch (NumberFormatException e)
                {
                    request.setAttribute(CommandConstants.ATTRIBUTE_FLAG, CommandConstants.FLAG_LUGGAGE_DELETE_ERROR);
                    request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_CLIENT_PAGE)).forward(request, response);
                }
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy()
    {

    }
}
