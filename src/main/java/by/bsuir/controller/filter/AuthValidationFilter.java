package by.bsuir.controller.filter;

import by.bsuir.business.service.CommandConstants;
import by.bsuir.util.global.Configurator;

import javax.servlet.*;
import java.io.IOException;

/**
 * Authentication validation filter.
 * This class is necessary for validating the authentication request.
 * @author Michail Sychevsky
 */
public class AuthValidationFilter implements Filter {

    public static final String REGEXP_LOGIN= "regexp_login";
    public static final String REGEXP_PASSWORD = "regexp_password";

    private String passwordPattern;
    private String loginPattern;

    @Override
    public void init(FilterConfig config) throws ServletException
    {
        passwordPattern = Configurator.getGlobalProperty(REGEXP_PASSWORD);
        loginPattern = Configurator.getGlobalProperty(REGEXP_LOGIN);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        String login = request.getParameter(CommandConstants.PARAMETER_LOGIN);
        String password = request.getParameter(CommandConstants.PARAMETER_PASSWORD);

        if(login != null && password != null)
        {
            if(!(login.matches(loginPattern) || password.matches(passwordPattern)))
            {
                request.setAttribute(CommandConstants.ATTRIBUTE_FLAG, CommandConstants.FLAG_AUTHORIZATION_FAILED);
                request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_START_PAGE)).forward(request, response);
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy()
    {

    }
}
