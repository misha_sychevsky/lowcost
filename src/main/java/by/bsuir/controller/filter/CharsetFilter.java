package by.bsuir.controller.filter;

import by.bsuir.util.global.Configurator;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * This class is necessary for coding translation in UTF-8.
 * @author Michail Sychevsky
 */
public class CharsetFilter implements Filter {

    public static final String CHARSET_TYPE = "charset";
    public static final String CONTENT_TYPE = "content_type";

    private String charset;
    private String content;

    public void init(FilterConfig config) throws ServletException
    {
        charset = Configurator.getGlobalProperty(CHARSET_TYPE);
        content = Configurator.getGlobalProperty(CONTENT_TYPE);
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        String encoding = request.getCharacterEncoding();
        if (!charset.equals(encoding))
        {
            request.setCharacterEncoding(charset);
        }
        response.setContentType(content);
        chain.doFilter(request, response);
    }

    public void destroy()
    {

    }
}
