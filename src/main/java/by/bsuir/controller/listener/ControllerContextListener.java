package by.bsuir.controller.listener;

import by.bsuir.business.util.TicketPriceUpdater;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Controller context listener.
 * This class starts the <code>TicketPriceUpdater</code> daemon which update ticket prices.
 * @author Michail Sychevsky
 */
public class ControllerContextListener implements ServletContextListener{

    private TicketPriceUpdater updater;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent)
    {
        if (updater == null)
        {
            updater = new TicketPriceUpdater();
            updater.start();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent)
    {
        if(updater != null)
        {
            updater.stop();
        }
    }
}
