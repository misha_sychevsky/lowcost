package by.bsuir.controller.servlet;

import by.bsuir.business.command.Command;
import by.bsuir.business.exception.NoSuchCommandException;
import by.bsuir.business.service.CommandConstants;
import by.bsuir.business.service.CommandFactory;
import by.bsuir.util.global.Configurator;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class implements the pattern MVC
 * This is Servlet which handles requests
 * @author Michail Sychevsky
 */
public class Controller extends HttpServlet {

    public static final long serialVersionUID = 1L;
    public static final Logger LOGGER = Logger.getLogger(Controller.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * This method gets a instance of <code>Command</code> from <code>CommandFactory</code>
     * by request and execute this command.
     * @param request a httpServletRequest
     * @param response a httpServletResponse
     * @throws ServletException a ServletException
     * @throws IOException a IOException
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            Command command = CommandFactory.getCommand(request.getParameter(CommandConstants.PARAMETER_COMMAND));
            command.execute(request, response);
        }
        catch (ServletException | IOException e)
        {
            LOGGER.error(e);
            request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_SERVER_ERROR)).forward(request, response);
        }
        catch (NoSuchCommandException e)
        {
            LOGGER.error(e);
            request.getRequestDispatcher(Configurator.getGlobalProperty(CommandConstants.FORWARD_NOT_FOUND_PAGE)).forward(request, response);
        }
    }
}
