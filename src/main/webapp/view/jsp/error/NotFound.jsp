<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib.tld" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <meta charset="utf-8">
  <title><tag:get-property uri="jsp.text.page.title.error"/></title>
  <link rel="icon" href="view/pictures/logo.ico">
  <link rel="stylesheet" href="view/css/Main.css">
</head>
<body>
<div class="main-container">
  <div class="logo">
    <img id="error-img" src="view/pictures/error.png">
    <div class="error-title-container">
      <p id="error-text-title"><tag:get-property uri="jsp.text.not-found-error"/></p>
      <p id="error-text">404</p>
    </div>
  </div>
  <div id="error-text-block">
    <p class="error-text">
      <tag:get-property uri="jsp.text.not-found-error.message.1"/>
    </p>
    <p class="error-text">
      <tag:get-property uri="jsp.text.not-found-error.message.2"/>
    </p>
  </div>
  <a href="../../../StartPage.jsp" id="main-ref">LowCosteR</a>
</div>
</body>
</html>
