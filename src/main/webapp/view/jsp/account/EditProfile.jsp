<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib.tld" %>
<html>
  <head>
    <title><tag:get-property uri="jsp.text.page.title.edit-profile"/></title>
    <link rel="icon" href="view/pictures/logo.ico">
    <link rel="stylesheet" href="view/css/Main.css">
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
  </head>
  <body>
    <c:if test="${flag != null && flag == 'editing_successful'}">
      <div class="authentication-failed-div" id="failed-div">
        <div class="authentication-failed">
          <img class="failed-img" src="view/pictures/Success_flag_128.png">
          <p class="failed-text ">
            <tag:get-property uri="jsp.text.editing-success"/>
          </p>
        </div>
      </div>
    </c:if>
    <c:if test="${flag != null && flag == 'editing_failed'}">
      <div class="authentication-failed-div" id="failed-div">
        <div class="authentication-failed">
          <img class="failed-img" src="view/pictures/warning38.png">
          <p class="failed-text ">
            <tag:get-property uri="jjsp.text.editing-failed.1"/>
          </p>
          <p class="failed-text">
            <tag:get-property uri="jsp.text.editing-failed.2"/>
          </p>
          <p class="failed-text">
            <tag:get-property uri="jsp.text.editing-failed.3"/>
          </p>
        </div>
      </div>
    </c:if>
    <div class="main-container">
      <div class="bar">
        <jsp:include page="../service/LocalizationSelect.jsp"/>
        <jsp:include page="../service/AccountNavigate.jsp"/>
      </div>
      <div class="logo-registration">
        <img class="logo-img" src="view/pictures/user-register.png">
        <div class="title-container">
          <p class="logo-text-title"><tag:get-property uri="jsp.text.logo.title.profile"/></p>
          <p class="logo-text">LowCosteR</p>
        </div>
      </div>
      <div class="registration-container">
        <form id="registration-form" action="Controller" method="post">
          <div id="first_name">
            <p class="reg-field-name">
              <tag:get-property uri="jsp.text.registration.field.first-name"/>
            </p>
            <input class="reg-field-text" type="text" name="firstName" value="${sessionScope.client.firstName}">
            <img class="reg-warning hidden" src="view/pictures/warning.png">
          </div>
          <div id="last_name">
            <p class="reg-field-name">
              <tag:get-property uri="jsp.text.registration.field.last-name"/>
            </p>
            <input class="reg-field-text" type="text" name="lastName" value="${sessionScope.client.lastName}">
            <img class="reg-warning hidden" src="view/pictures/warning.png">
          </div>
          <div>
            <p class="reg-field-name">
              <tag:get-property uri="jsp.text.registration.field.birth-date"/>
            </p>
            <input class="reg-field-text" type="date" name="birthDate" value="${sessionScope.client.birthDate}">
          </div>
          <div id="phone">
            <p class="reg-field-name">
              <tag:get-property uri="jsp.text.registration.field.phone"/>
            </p>
            <input class="reg-field-text" type="text" name="phone" value="${sessionScope.client.phone}">
            <img class="reg-warning hidden" src="view/pictures/warning.png">
          </div>
          <div id="password">
            <p class="reg-field-name">
              <tag:get-property uri="jsp.text.registration.field.password"/>
            </p>
            <input class="reg-field-text" type="text" name="password" value="${sessionScope.client.password}">
            <img class="reg-warning hidden" src="view/pictures/warning.png">
          </div>
          <div id="passport">
            <p class="reg-field-name">
              <tag:get-property uri="jsp.text.registration.field.passport"/>
            </p>
            <input class="reg-field-text" type="text" name="passport" value="${sessionScope.client.passportNS}">
            <img class="reg-warning hidden" src="view/pictures/warning.png">
          </div>
          <div id="country">
            <p class="reg-field-name">
              <tag:get-property uri="jsp.text.registration.field.country"/>
            </p>
            <input class="reg-field-text" type="text" name="country" value="${sessionScope.client.country}">
            <img class="reg-warning hidden" src="view/pictures/warning.png">
          </div>
          <div class="button-container">
            <input type="hidden" name="command" value="edit_profile"/>
            <div id="edit-profile" class="edit-button reg-button login-button">
              <tag:get-property uri="jsp.text.edit.button"/>
            </div>
          </div>
        </form>
      </div>
      <a href="../../../StartPage.jsp" id="main-ref">LowCosteR</a>
    </div>
    <script src="view/js/scripts.js"></script>
  </body>
</html>
