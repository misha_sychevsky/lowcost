<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib.tld" %>
<html>
  <head>
    <title><tag:get-property uri="jsp.text.page.title.registration-profile"/></title>
    <link rel="icon" href="view/pictures/logo.ico">
    <link rel="stylesheet" href="view/css/Main.css">
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
  </head>
    <body>
    <c:if test="${flag != null && flag == 'registration_failed'}">
      <div class="authentication-failed-div" id="failed-div">
        <div class="authentication-failed">
          <img class="failed-img" src="view/pictures/warning38.png">
          <p class="failed-text ">
            <tag:get-property uri="jsp.text.registration-failed.1"/>
          </p>
          <p class="failed-text">
            <tag:get-property uri="jsp.text.registration-failed.2"/>
          </p>
          <p class="failed-text">
            <tag:get-property uri="jsp.text.registration-failed.3"/>
          </p>
        </div>
      </div>
    </c:if>
    <c:if test="${flag != null && flag == 'login_exists'}">
      <div class="authentication-failed-div" id="failed-div">
        <div class="authentication-failed">
          <img class="failed-img" src="view/pictures/warning38.png">
          <p class="failed-text ">
            <tag:get-property uri="jsp.text.login-alert"/>
          </p>
        </div>
      </div>
    </c:if>
    <div class="main-container">
      <div class="bar">
        <jsp:include page="../service/LocalizationSelect.jsp"/>
      </div>
      <div class="logo-registration">
        <img class="logo-img" src="view/pictures/user-register.png">
        <div class="title-container">
          <p class="logo-text-title"><tag:get-property uri="jsp.text.logo.title.registration"/></p>
          <p class="logo-text">LowCosteR</p>
        </div>
      </div>
      <div class="registration-container">
        <form id="registration-form" action="Controller" method="post">
          <div id="first_name">
            <p class="reg-field-name">
              <tag:get-property uri="jsp.text.registration.field.first-name"/>
            </p>
            <input class="reg-field-text" type="text" name="firstName">
            <img class="reg-warning hidden" src="view/pictures/warning.png">
          </div>
          <div id="last_name">
            <p class="reg-field-name">
              <tag:get-property uri="jsp.text.registration.field.last-name"/>
            </p>
            <input class="reg-field-text" type="text" name="lastName">
            <img class="reg-warning hidden" src="view/pictures/warning.png">
          </div>
          <div>
            <p class="reg-field-name">
              <tag:get-property uri="jsp.text.registration.field.birth-date"/>
            </p>
            <input class="reg-field-text" id="date" type="date" name="birthDate" value="2015-01-01">
          </div>
          <div id="phone">
            <p class="reg-field-name">
              <tag:get-property uri="jsp.text.registration.field.phone"/>
            </p>
            <input class="reg-field-text" type="text" name="phone">
            <img class="reg-warning hidden" src="view/pictures/warning.png">
          </div>
          <div id="email">
            <p class="reg-field-name">
              <tag:get-property uri="jsp.text.registration.field.email"/>
            </p>
            <input class="reg-field-text" type="text" name="email">
            <img class="reg-warning hidden" src="view/pictures/warning.png">
          </div>
          <div id="login">
            <p class="reg-field-name">
              <tag:get-property uri="jsp.text.registration.field.login"/>
            </p>
            <input class="reg-field-text" type="text" name="login">
            <img class="reg-warning hidden" src="view/pictures/warning.png">
          </div>
          <div id="password">
            <p class="reg-field-name">
              <tag:get-property uri="jsp.text.registration.field.password"/>
            </p>
            <input class="reg-field-text" type="text" name="password">
            <img class="reg-warning hidden" src="view/pictures/warning.png">
          </div>
          <div id="passport">
            <p class="reg-field-name">
              <tag:get-property uri="jsp.text.registration.field.passport"/>
            </p>
            <input class="reg-field-text" type="text" name="passport">
            <img class="reg-warning hidden" src="view/pictures/warning.png">
          </div>
          <div id="country">
            <p class="reg-field-name">
              <tag:get-property uri="jsp.text.registration.field.country"/>
            </p>
            <input class="reg-field-text" type="text" name="country">
            <img class="reg-warning hidden" src="view/pictures/warning.png">
          </div>
          <input type="hidden" name="command" value="registration"/>
          <div class="button-container">
            <div id="registration" class="reg-button login-button edit-button">
              <tag:get-property uri="jsp.text.registration.button"/>
            </div>
          </div>
        </form>
      </div>
      <a href="../../../StartPage.jsp" id="main-ref">LowCosteR</a>
    </div>
    <script src="view/js/scripts.js"></script>
  </body>
</html>
