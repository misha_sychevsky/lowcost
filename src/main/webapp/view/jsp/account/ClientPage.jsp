<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title><tag:get-property uri="jsp.text.page.title.home"/></title>
    <link rel="icon" href="view/pictures/logo.ico">
    <link rel="stylesheet" href="view/css/Table.css">
    <link rel="stylesheet" href="view/css/Main.css">
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
  </head>
  <body>
    <c:if test="${flag != null && (flag == 'ticket_delete_error' || flag == 'luggage_delete_error')}">
      <div class="authentication-failed-div" id="failed-div">
        <div class="authentication-failed">
          <img class="failed-img" src="view/pictures/Success_flag_128.png">
          <p class="failed-text ">
            <tag:get-property uri="jsp.text.delete-error.1"/>
          </p>
          <p class="failed-text ">
            <tag:get-property uri="jsp.text.delete-error.2"/>
          </p>
        </div>
      </div>
    </c:if>
    <div class="main-container">
      <div class="bar">
        <jsp:include page="../service/LocalizationSelect.jsp"/>
        <a href="Controller?command=forward_to_page&forwardPage=view/jsp/account/EditProfile.jsp" class="edit"></a>
        <div class="user-container">
          <form action="Controller" method="post">
            <input type="hidden" name="command" value="exit"/>
            <input class="user exit" type="submit" name="exit" value="">
          </form>
        </div>
      </div>
      <div id="user-logo">
        <img id="user-img" src="view/pictures/man.png">
        <div class="title-container">
          <p id="user-title">${sessionScope.client.firstName}</p>
          <p id="user-text">${sessionScope.client.lastName}</p>
        </div>
      </div>
      <form id="ticket-delete-form" action="Controller" method="post" class="table-container">
        <c:choose>
          <c:when test="${tickets_flag != null && tickets_flag == 'no_tickets'}">
            <p class="table-title">
              <tag:get-property uri="jsp.text.no-tickets.1"/>
            </p>
            <p class="table-title">
              <tag:get-property uri="jsp.text.no-tickets.2"/>
            </p>
          </c:when>
          <c:otherwise>
            <p class="table-title">
              <tag:get-property uri="jsp.text.tickets"/>
            </p>
            <table id="tickets_table" class="simple-little-table">
              <thead>
                <tr>
                  <th><tag:get-property uri="jsp.text.table.flight"/></th>
                  <th><tag:get-property uri="jsp.text.table.class"/></th>
                  <th><tag:get-property uri="jsp.text.table.seat"/></th>
                  <th><tag:get-property uri="jsp.text.table.from-to"/></th>
                  <th><tag:get-property uri="jsp.text.table.time"/></th>
                  <th><tag:get-property uri="jsp.text.table.date"/></th>
                  <th><tag:get-property uri="jsp.text.table.privilege"/></th>
                  <th><tag:get-property uri="jsp.text.table.price"/></th>
                  <th><img class="check-column" src="view/pictures/check.png"></th>
                </tr>
              </thead>
              <tbody id="ticket-table-body">
                <jsp:include page="../service/PrintTicketPage.jsp"/>
              </tbody>
            </table>
            <div class="table-bar">
              <input type="hidden" name="command" value="delete_tickets"/>
              <div id="prev-ticket-page" class="table-button navigate-button navigate-button-prev"></div>
              <div id="next-ticket-page" class="table-button navigate-button navigate-button-next"></div>
              <div id="delete-tickets" class="table-button login-button edit-button">
                <tag:get-property uri="jsp.text.delete.button"/>
              </div>
            </div>
          </c:otherwise>
        </c:choose>
      </form>
      <form id="luggage-delete-form" action="Controller" method="post" class="table-container">
        <c:choose>
          <c:when test="${luggage_flag != null && luggage_flag == 'no_luggage'}">
            <p class="table-title">
              <tag:get-property uri="jsp.text.no-luggage"/>
            </p>
          </c:when>
          <c:otherwise>
            <p class="table-title">
              <tag:get-property uri="jsp.text.luggage"/>
            </p>
            <table class="simple-little-table luggage-table">
              <thead>
                <tr>
                  <th><tag:get-property uri="jsp.text.table.luggage"/></th>
                  <th><tag:get-property uri="jsp.text.table.weight"/></th>
                  <th><tag:get-property uri="jsp.text.table.dimensions"/></th>
                  <th><tag:get-property uri="jsp.text.table.flight"/></th>
                  <th><tag:get-property uri="jsp.text.table.from-to"/></th>
                  <th><tag:get-property uri="jsp.text.table.date"/></th>
                  <th><tag:get-property uri="jsp.text.table.price"/></th>
                  <th><img class="check-column" src="view/pictures/check.png"></th>
                </tr>
              </thead>
              <tbody id="luggage-table-body">
                <jsp:include page="../service/PrintLuggagePage.jsp"/>
              </tbody>
            </table>
            <div class="table-bar">
              <input type="hidden" name="command" value="delete_luggage"/>
              <div id="prev-luggage-page" class="table-button navigate-button navigate-button-prev"></div>
              <div id="next-luggage-page" class="table-button navigate-button navigate-button-next"></div>
              <div id="delete-luggage" class="table-button login-button edit-button">
                <tag:get-property uri="jsp.text.delete.button"/>
              </div>
            </div>
          </c:otherwise>
        </c:choose>
      </form>
      <a href="../../../StartPage.jsp" id="main-ref">LowCosteR</a>
    </div>
    <script src="view/js/scripts.js"></script>
  </body>
</html>
