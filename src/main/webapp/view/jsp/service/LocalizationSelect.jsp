<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="localization">
  <form action="Controller" method="get">
    <select onchange="this.form.submit()" name="language" class="lang-select">
      <option <c:if test="${sessionScope.language == 'EN'}">selected</c:if>>EN</option>
      <option <c:if test="${sessionScope.language == 'RU'}">selected</c:if>>RU</option>
    </select>
    <input type="hidden" name="command" value="change_language"/>
  </form>
</div>
