<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib.tld" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <meta charset="utf-8">
    <title><tag:get-property uri="jsp.text.page.title.flights"/></title>
    <link rel="icon" href="view/pictures/logo.ico">
    <link rel="stylesheet" href="view/css/Main.css">
    <link rel="stylesheet" href="view/css/Table.css">
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
  </head>
  <body>
    <div class="main-container">
      <div class="bar">
        <jsp:include page="../service/LocalizationSelect.jsp"/>
        <jsp:include page="../service/AccountNavigate.jsp"/>
      </div>
      <div class="logo-flight">
        <img class="logo-img flight-img" src="view/pictures/flight.png">
        <div class="flight-title-container">
          <p class="logo-text-title"><tag:get-property uri="jsp.text.logo.title.flights"/></p>
          <p class="logo-text"><tag:get-property uri="jsp.text.logo.title.tickets"/></p>
        </div>
      </div>
      <div class="flights-container">
        <div class="flights-container-child">
          <div class="titles-flights">
            <p class="flight-data city-from">
              <tag:get-property uri="jsp.text.header.from"/>
              <span class="city-from-name">${param.from}</span>
            </p>
            <p class="flight-data city-to">
              <tag:get-property uri="jsp.text.header.to"/>
              <span class="city-from-name">${param.to}</span>
            </p>
            <p class="flight-data date">
              <tag:get-property uri="jsp.text.header.date"/>
              <span class="flight-date">${param.date}</span>
            </p>
          </div>
          <img id="ticket-img" src="view/pictures/ticket.png">
          <form action="Controller" method="post" id="flights">
            <table class="simple-little-table">
              <thead>
                <tr>
                  <th><tag:get-property uri="jsp.text.table.flight"/></th>
                  <th><tag:get-property uri="jsp.text.table.departure"/></th>
                  <th><tag:get-property uri="jsp.text.table.arrival"/></th>
                  <th><tag:get-property uri="jsp.text.table.class"/></th>
                  <th><tag:get-property uri="jsp.text.table.price"/></th>
                  <c:if test="${sessionScope.privilege == true}">
                    <th><tag:get-property uri="jsp.text.table.privilege"/></th>
                  </c:if>
                  <th><tag:get-property uri="jsp.text.table.select"/></th>
                </tr>
              </thead>
              <tbody id="flights-table-body">
                <jsp:include page="../service/PrintFlightPage.jsp"/>
              </tbody>
            </table>
            <div class="button-container">
              <input id="command-flights" type="hidden" name="command" value="show_selected_ticket"/>
              <div id="prev-flights-page" class="table-button navigate-button navigate-button-prev"></div>
              <div id="next-flights-page" class="table-button navigate-button navigate-button-next"></div>
              <div id="order" class="reg-button login-button edit-button">
                <tag:get-property uri="jsp.text.table.order.button"/>
              </div>
            </div>
          </form>
        </div>
      </div>
      <a href="../../../StartPage.jsp" id="main-ref">LowCosteR</a>
    </div>
    <script src="view/js/scripts.js"></script>
  </body>
</html>
