<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>LowCosteR</title>
        <link rel="icon" href="view/pictures/logo.ico">
        <link rel="stylesheet" href="view/css/Main.css">
        <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    </head>
    <body>
        <c:if test="${flag != null && flag == 'no_flights'}">
            <div class="authentication-failed-div" id="failed-div">
                <div class="authentication-failed">
                    <img class="failed-img" src="view/pictures/not_found.png">
                    <p class="failed-text">
                        <tag:get-property uri="jsp.text.search-failed"/>
                    </p>
                </div>
            </div>
        </c:if>
        <c:if test="${flag != null && flag == 'ticket_order_successful'}">
            <div class="authentication-failed-div" id="failed-div">
                <div class="authentication-failed">
                    <img class="failed-img" src="view/pictures/Success_flag_128.png">
                    <p class="failed-text ">
                        <tag:get-property uri="jsp.text.order-success.1"/>
                    </p>
                    <p class="failed-text">
                        <tag:get-property uri="jsp.text.order-success.2"/>
                    </p>
                    <p class="failed-text">
                        <tag:get-property uri="jsp.text.order-success.3"/>
                    </p>
                </div>
            </div>
        </c:if>
        <c:if test="${flag != null && flag == 'registration_successful'}">
            <div class="authentication-failed-div" id="failed-div">
                <div class="authentication-failed">
                    <img class="failed-img" src="view/pictures/Success_flag_128.png">
                    <p class="failed-text ">
                        <tag:get-property uri="jsp.text.registration-success.1"/>
                    </p>
                    <p class="failed-text">
                        <tag:get-property uri="jsp.text.registration-success.2"/>
                    </p>
                </div>
            </div>
        </c:if>
        <c:if test="${flag != null && flag == 'authorization_failed'}">
            <div class="authentication-failed-div" id="failed-div">
                <div class="authentication-failed">
                    <img class="failed-img" src="view/pictures/warning38.png">
                    <p class="failed-text ">
                        <tag:get-property uri="jsp.text.authorization-failed.1"/>
                    </p>
                    <p class="failed-text">
                        <tag:get-property uri="jsp.text.authorization-failed.2"/>
                    </p>
                </div>
            </div>
        </c:if>
        <div class="form-login hidden" id="form-login-div">
            <form id="login-form" action="Controller" method="post" class="form-login-container">
                <div class="form-login-title">
                    <h2><tag:get-property uri="jsp.text.authorization.title"/></h2>
                </div>
                <div class="form-login-title">
                    <tag:get-property uri="jsp.text.authorization.field.login"/>
                </div>
                <input id="login-field" class="form-field" type="text" name="login" />
                <br />
                <div class="form-login-title">
                    <tag:get-property uri="jsp.text.authorization.field.password"/>
                </div>
                <input id="password-field" class="form-field" type="password" name="password" />
                <br />
                <div class="login-container">
                    <input type="hidden" name="command" value="authorization"/>
                    <div id="login" class="login-button">
                        <tag:get-property uri="jsp.text.authorization.button"/>
                    </div>
                </div>
            </form>
        </div>
        <div class="main-container">
            <div class="bar">
                <jsp:include page="view/jsp/service/LocalizationSelect.jsp"/>
                <c:choose>
                    <c:when test="${sessionScope.client == null}">
                        <div class="login"></div>
                    </c:when>
                    <c:otherwise>
                        <jsp:include page="view/jsp/service/AccountNavigate.jsp"/>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="logo">
                <img class="logo-img" src="view/pictures/airplane-plane.png">
                <div class="title-container">
                    <p class="logo-text-title">LowCosteR</p>
                    <p class="logo-text">Low Cost Airlines</p>
                </div>
            </div>
            <div class="panes-container">
                <div class="finder-pane">
                    <c:choose>
                        <c:when test="${sessionScope.client == null}">
                            <div class="register-alert">
                                <img src="view/pictures/user-none.png">
                                <p style="padding-bottom: 10px">
                                    <tag:get-property uri="jsp.text.registration-alert"/>
                                </p>
                                <a class="registration" href="Controller?command=forward_to_page&forwardPage=view/jsp/account/Registration.jsp">
                                    <tag:get-property uri="jsp.text.registration-alert.button"/>
                                </a>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <form id="find-form" class="form-finder-container" action="Controller" method="post">
                                <div class="form-finder-title">
                                    <tag:get-property uri="jsp.text.search-form.field.from"/>
                                </div>
                                <input id="from" class="form-field" type="text" name="from">
                                <div class="form-finder-title">
                                    <tag:get-property uri="jsp.text.search-form.field.to"/>
                                </div>
                                <input id="to" class="form-field" type="text" name="to">
                                <div class="form-finder-title">
                                    <tag:get-property uri="jsp.text.search-form.field.date"/>
                                </div>
                                <input class="form-field" type="date" name="date" value="2015-06-20">
                                <div class="form-finder-title">
                                    <tag:get-property uri="jsp.text.search-form.field.class"/>
                                </div>
                                <select class="form-field" name="classType">
                                    <option value="Economy">
                                        <tag:get-property uri="jsp.text.search-form.field.class.value.1"/>
                                    </option>
                                    <option value="Comfort">
                                        <tag:get-property uri="jsp.text.search-form.field.class.value.2"/>
                                    </option>
                                    <option value="Business">
                                        <tag:get-property uri="jsp.text.search-form.field.class.value.3"/>
                                    </option>
                                </select>
                                <span class="check-text">
                                    <input type="checkbox" name="privilege">
                                    <tag:get-property uri="jsp.text.search-form.field.privilege"/>
                                </span>
                                <div class="finder-container">
                                    <input type="hidden" name="command" value="flights_search"/>
                                    <div id="find" class="finder-button">
                                        <tag:get-property uri="jsp.text.search-form.button"/>
                                    </div>
                                </div>
                            </form>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="info-pane">
                    <img class="info-pane-img" src="view/pictures/samolet.jpg">
                    <p class="info-text">
                        <tag:get-property uri="jsp.text.info-pane"/>
                    </p>
                    <form id="luggage-price-form" action="Controller" method="get">
                        <input type="hidden" name="command" value="show_luggage_information"/>
                        <div id="luggage-price-button" class="luggage-price">
                            <tag:get-property uri="jsp.text.ref.luggage-prices"/>
                        </div>
                    </form>
                </div>
            </div>
            <div class="text-block">
                <p class="text">
                    <tag:get-property uri="jsp.text.text-block.1"/>
                </p>
                <p class="text">
                    <tag:get-property uri="jsp.text.text-block.2"/>
                </p>
                <p class="text">
                    <tag:get-property uri="jsp.text.text-block.3"/>
                </p>
                <p class="text">
                    <tag:get-property uri="jsp.text.text-block.4"/>
                </p>
                <p class="text">
                    <tag:get-property uri="jsp.text.text-block.5"/>
                </p>
            </div>
            <div class="partners">
                <img class="partners-img" src="view/pictures/delta-partner.png">
                <img class="partners-img" src="view/pictures/bpeing-partner.png">
                <img class="partners-img" src="view/pictures/lowco-partner.png">
                <img class="partners-img" src="view/pictures/Logo%20United_Airlines.png">
                <img class="partners-img" src="view/pictures/cheapfl-partner.png">
            </div>
        </div>
        <script src="view/js/scripts.js"></script>
    </body>
</html>
